TCGA_primary_site	TCGA_exomes	COSMIC_primary_site	COSMIC_samples_with_CTNNB1_missense_mutations
adrenal gland	240	adrenal_gland	251
gallbladder +  other and unspecified parts of biliary tract	7	biliary_tract	94
liver and intrahepatic bile ducts    419   liver      1311
corpus uteri	       545  endometrium	   480
kidney 695	       kidney	   200
bronchus and lung      1067	   lung	154
ovary	 427 ovary     152
pancreas 183 pancreas  267
colon	 434 large_intestine	321
prostate gland	498	prostate	114
stomach	 447	stomach	192
skin	 470	skin	210
brain	 911	central_nervous_system	298
other endocrine glands and related structures	1	pituitary	368
connective, subcutaneous and other soft tissues	126	soft_tissue	2016
