# README

Data collection and analysis code, documentation, and plots.

Krishnan A, Meynert A, Kelder M, Ewing A, Sheraz S, Ferrer-Vaquer A, Grimes G, Becher H, Silk R, Semple CA, Kendall T, Hadjantonakis A, Bird T, Marsh JA, Hohenstein P, Wood AJ, and Ozdemir D. **Mutational scanning reveals oncogenic CTNNB1 mutations have diverse effects on signalling and clinical traits.**

Corresponding authors:
- andrew.j.wood @ ed.ac.uk
- p.hohenstein @ lumc.nl
- deozdemir @ ku.edu.tr

## Data collection

### COSMIC
Targeted and genome-wide mutations were downloaded from https://cancer.sanger.ac.uk/cosmic/download, filtered by gene CTNNB1, from COSMIC release v94 (28 May 2021). 

- https://git.ecdf.ed.ac.uk/igmmbioinformatics/betacatenin-saturation-screen/-/blob/master/docs/cosmic.md.

### TCGA
TCGA samples for calculating mutational frequency by tri-nucleotide context and subsequent mutational likelihood scores were selected via the Genomic Data Commons Repository (GDC: https://portal.gdc.cancer.gov/repository) and downloaded with the gdc-client 1.6.1. Initial selection criteria were for data category ‘simple nucleotide variation’, data format ‘vcf’, experimental strategy ‘WXS’, and all workflows (MuSE, MuTect2, SomaticSniper, and VarScan2). Liver cancer samples were selected via criteria primary site ‘liver and intrahepatic bile ducts’ and project id ‘TCGA-LIHC’; uterine endometrial cancer samples via primary site ‘corpus uteri’ and project id ‘TCGA-UCEC’. Duplicate samples were removed, and variants identified by only a single workflow were excluded. Uterine endometrial cancer samples with subtype ‘cystic, mucinous and serous neoplasms’ were excluded.

- https://git.ecdf.ed.ac.uk/igmmbioinformatics/betacatenin-saturation-screen/-/blob/master/docs/tcga.md

TCGA samples with high or moderate impact mutations in CTNNB1 were selected from the GDC. MAF files for Mutect2 VCFs and expression quantification files were downloaded with the gdc-client.

- https://git.ecdf.ed.ac.uk/igmmbioinformatics/betacatenin-saturation-screen/-/blob/master/docs/tcga_ctnnb1_mutation_and_cdh1_expression.md


## Data analysis

All data analysis is documented and custom scripts available at https://git.ecdf.ed.ac.uk/igmmbioinformatics/betacatenin-saturation-screen.

### Read processing

Reads from the saturation screen were quality checked with FastQC version v0.11.8 and a report generated with MultiQC 1.10.1. Reads were processed in a Nextflow DLS1 pipeline. Adapters were trimmed and read pairs merged with NGmerge 0.3, the resulting single end reads aligned against the provided reference sequence for the CTNNB1 hotspot with bwa 0.1.17 and alignment files sorted and indexed with samtools 1.12. Mutations were extracted via a custom Perl script.

- https://git.ecdf.ed.ac.uk/igmmbioinformatics/betacatenin-saturation-screen/-/blob/master/analysis/qc/fastqc_multiqc_report.html
- https://git.ecdf.ed.ac.uk/igmmbioinformatics/betacatenin-saturation-screen/-/blob/master/docs/read_processing.md
- NGmerge: https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-018-2579-2
- bwa: https://academic.oup.com/bioinformatics/article/25/14/1754/225615
- samtools: https://doi.org/10.1093/gigascience/giab008

### Normalization of sample pools

Replicates correlated strongly and were merged. Pools were normalized against the unselected pool using a custom R 4.1.10 script.

- https://git.ecdf.ed.ac.uk/igmmbioinformatics/betacatenin-saturation-screen/-/blob/master/docs/replicate_correlation.md
- https://git.ecdf.ed.ac.uk/igmmbioinformatics/betacatenin-saturation-screen/-/blob/master/docs/pool_vs_plasmid.md
- https://git.ecdf.ed.ac.uk/igmmbioinformatics/betacatenin-saturation-screen/-/blob/master/docs/normalized_pools.md

## Mutational likelihood scores

Mutations in the CTNNB1 hotspot were extracted from TCGA samples via a series of custom scripts. Background frequencies in a tri-nucleotide context were calculated with an R script using the VariantAnnotation and SomaticSignatures packages. All codon to codon mutation paths were enumerated and their likelihoods were calculated for the LIHC and UCEC cancer types separately using a custom Python script, and summarized with a custom R script.

- https://git.ecdf.ed.ac.uk/igmmbioinformatics/betacatenin-saturation-screen/-/blob/master/docs/tcga.md
