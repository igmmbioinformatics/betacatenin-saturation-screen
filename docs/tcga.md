# TCGA somatic variants

## Scripts

* [tcga_extract_ctnnb1_variants.sh](../scripts/tcga_extract_ctnnb1_variants.sh)
* [tcga_extract_all_variants.sh](../scripts/tcga_extract_all_variants.sh)
* [ensembl_variant_calls.pl](../scripts/ensembl_variant_calls.pl)
* [tcga_check_duplicate_cases.sh](../scripts/tcga_check_duplicate_cases.sh)
* [tcga_missense_in_target.R](../scripts/tcga_missense_in_target.R)
* [tcga_missense.R](../scripts/tcga_missense.R)
* [tcga_plot_missense_counts.R](../scripts/tcga_plot_missense_counts.R)
* [tcga_trinuc_context_mutation_freqs.R](../scripts/tcga_trinuc_context_mutation_freqs.R)

## Data acquisition

Searching on https://portal.gdc.cancer.gov/repository.

### Hepatocellular carcinoma

Search terms

```
Primary Site IS liver and intrahepatic bile ducts
AND
Workflow Type IN (MuSE Annotation, MuTect2 Annotation, SomaticSniper Annotation, VarScan2 Annotation)
AND
Data Category IS simple nucleotide variation
AND
Data Format IS vcf
AND
Experimental Strategy IS WXS
AND
Project Id IS TCGA-LIHC
```

* 1512 files
* 378 files per workflow
* 375 cases
* Manifest saved to `data/tcga/gdc_manifest.2021-09-14_liver.txt`
* Files JSON saved to `data/tcga/files.2021-09-15_liver.json`

### Uterine endometrial carcinoma

Search terms

```
Primary Site IS corpus uteri
AND
Workflow Type IN (MuSE Annotation, MuTect2 Annotation, SomaticSniper Annotation, VarScan2 Annotation)
AND
Data Category IS simple nucleotide variation
AND
Data Format IS vcf
AND
Experimental Strategy IS WXS
AND
Project Id IS TCGA-UCEC
```

* 2244 files
* 561 files per workflow
* 542 cases
* Manifest saved to `data/tcga/gdc_manifest.2021-09-14_uterine.txt`
* Files JSON saved to `data/tcga/files.2021-09-15_uterine.json`

### Download software

**Note:** The software directory is not included in the repository.

```
cd software
wget https://gdc.cancer.gov/files/public/file/gdc-client_v1.6.1_Ubuntu_x64.zip
unzip gdc-client_v1.6.1_Ubuntu_x64.zip
rm gdc-client_v1.6.1_Ubuntu_x64.zip
```

### Download files

The files in the manifests listed were downloaded by Martin Taylor. `.vcf.gz` and `.vcf.gz.tbi` files were moved to the following:

* Liver: `data/tcga/liver/raw`
* Uterine: `data/tcga/uterine/raw`

Unindexed files were tabix'd.

```
for file in *.gz
do
  if [ ! -e $file.tbi ]
  then
    tabix $file
  fi
done
```

All other downloaded files were moved to the following:

* Liver: `data/tcga/liver/download`
* Uterine: `data/tcga/uterine/download`

### Map case ids to file ids

```
cd data/tcga
python ../../scripts/parse_gdc_files_json.py files.2021-09-15_liver.json | sed -e 's/\.vep\.vcf\.gz//' > files.2021-09-15_liver.txt
python ../../scripts/parse_gdc_files_json.py files.2021-09-15_uterine.json | sed -e 's/\.vep\.vcf\.gz//' > files.2021-09-15_uterine.txt
```

## Data extraction - variants in CTNNB1 hotspot

### Extract only variants in CTNNB1

```
cd liver
../../../scripts/tcga_extract_ctnnb1_variants.sh
cd ../uterine
../../../scripts/tcga_extract_ctnnb1_variants.sh
```

### Check duplicate cases

```
cd data/tcga
../../scripts/tcga_check_duplicate_cases.sh
```

```
Liver duplicate cases
     12 d6486001-240a-455a-980c-e06c25c61fa5
      8 7fe5e2d9-c514-47bc-8387-3f940f7a822d

Uterine duplicate cases
     32 8d9e4917-334b-4c76-aee1-1e22be772db0
     24 ab7adf74-6e87-4117-9742-54e323348e06
     24 51bd85e2-2c60-4654-bfcb-83dccef8964b
     12 2b865f8c-539f-4cb2-8f08-41709e9511e5
```

* d6486001-240a-455a-980c-e06c25c61fa5 has the same single variant in all 12 files, plus 1 extra only in 1 file
* 7fe5e2d9-c514-47bc-8387-3f940f7a822d has no variants in any file
* 8d9e4917-334b-4c76-aee1-1e22be772db0 has one variant that shows up in 2 files, 2bp deletion
* ab7adf74-6e87-4117-9742-54e323348e06 is a mess:
  * 3 variants each showing up in only 1 file
  * 1 variant showing up in 2 files, same 2bp deletion as 8d9e4917-334b-4c76-aee1-1e22be772db0
* 51bd85e2-2c60-4654-bfcb-83dccef8964b:
  * 1 variant showing up in 4 files
  * 2 showing up in 1 file only each
* 2b865f8c-539f-4cb2-8f08-41709e9511e5: 2 variants showing up in 1 file only each

### Identify variants called by 2 of 4 somatic workflows

```
perl ../../scripts/ensembl_variant_calls.pl --input files.2021-09-15_liver.txt --vcfs liver --output ../../analysis/tcga/liver_variants.txt
perl ../../scripts/ensembl_variant_calls.pl --input files.2021-09-15_uterine.txt --vcfs uterine --output ../../analysis/tcga/uterine_variants.txt
```

### Filter to missense variants in target region

```
cd ../../analysis/tcga
Rscript ../../scripts/tcga_missense_in_target.R
```

### Checking cases by type - endometrial

* Adenomas and adenocarcinomas - 402
* Cystic, mucinous and serous neoplasms - 138
* Epthelial neoplasms, nos - 2

Downloaded case ids from GDC for each of these, files of format `case_set_...2021-09-16.tsv`. Virtually all of the variants are in the adenomas & adenocarcinomas.

```
for file in ../../data/tcga/case_set*
do
  echo `grep -v id uterine_missense_variants.csv | grep -c -f $file ` $file
done

105 case_set_adenomas_and_adenocarcinomas__TCGA__TCGA_UCEC__simple_nucleotide_variation__Annotated_Somatic_Mut....2021-09-16.tsv
1 case_set_cystic__mucinous_and_serous_neoplasms__TCGA__TCGA_UCEC__simple_nucleotide_variation__Annotated_So....2021-09-16.tsv
2 case_set_epithelial_neoplasms__nos__TCGA__TCGA_UCEC__simple_nucleotide_variation__Annotated_Somatic_Mutation.2021-09-16.tsv
```

Decision: exclude cystic/mucinous/serous neoplasms.

```
grep -v id ../../data/tcga/case_set_cystic__mucinous_and_serous_neoplasms__TCGA__TCGA_UCEC__simple_nucleotide_variation__Annotated_So....2021-09-16.tsv > temp.out
grep -v -f temp.out uterine_missense_variants.csv > uterine_missense_variants_filtered.csv
rm temp.out
```

### Plot counts

* [missense_counts.csv](../analysis/tcga/missense_counts.csv)

```
Rscript tcga_plot_missense_counts.R
```

![Endometrial uterine carcinoma mutation counts](../analysis/tcga/endometrial_missense_mutations.png "Endometrial uterine carcinoma mutation counts")

![Liver carcinoma mutation counts](../analysis/tcga/liver_missense_mutations.png "Liver carcinoma mutation counts")

## Data extraction - all variants for mutational likelihood scores

```
cd liver
../../../scripts/tcga_extract_all_variants.sh
cd ../uterine
../../../scripts/tcga_extract_all_variants.sh
```

### Identify variants called by 2 of 4 somatic workflows

```
perl ../../scripts/ensembl_variant_calls.pl --input files.2021-09-15_liver.txt --vcfs liver/raw --output ../../analysis/mutation_likelihood_scores/LIHC_all_variants.txt --vcf_suffix .vep.vcf.gz --txt_suffix .txt
perl ../../scripts/ensembl_variant_calls.pl --input files.2021-09-15_uterine.txt --vcfs uterine/raw --output ../../analysis/mutation_likelihood_scores/UCEC_all_variants.txt --vcf_suffix .vep.vcf.gz --txt_suffix .txt
```

### Filter to missense variants

```
cd ../../analysis/mutation_likelihood_scores
Rscript ../../scripts/tcga_missense.R
```

### Filter by subtype - endometrial

Decision: exclude cystic/mucinous/serous neoplasms.

```
grep -v id ../../data/tcga/case_set_cystic__mucinous_and_serous_neoplasms__TCGA__TCGA_UCEC__simple_nucleotide_variation__Annotated_So....2021-09-16.tsv > temp.out
grep -v -f temp.out UCEC_all_missense_variants.csv > UCEC_all_missense_variants_filtered.csv
rm temp.out
```

## Mutation likelihood scores

### Tri-nucleotide mutation frequency

```
mkdir previous
mkdir hotspot_cases
mkdir non_hotspot_cases

cut -f 1 -d ',' ../tcga/liver_missense_variants.csv | sort -u > LIHC_cases_with_hotspot_missense_variants.txt
cut -f 1 -d ',' ../tcga/uterine_missense_variants_filtered.csv | sort -u > UCEC_cases_with_hotspot_missense_variants.txt
```

Copy previous background frequencies (from hotspot cases only) into `previous`.

Divide up missense variants by hotspot case or not.

```
grep -f LIHC_cases_with_hotspot_missense_variants.txt LIHC_all_missense_variants.csv > hotspot_cases/LIHC_missense_variants.csv
head -n 1 LIHC_all_missense_variants.csv > non_hotspot_cases/LIHC_missense_variants.csv
grep -v -f LIHC_cases_with_hotspot_missense_variants.txt LIHC_all_missense_variants.csv >> non_hotspot_cases/LIHC_missense_variants.csv
grep -f UCEC_cases_with_hotspot_missense_variants.txt UCEC_all_missense_variants_filtered.csv > hotspot_cases/UCEC_missense_variants.csv
head -n 1 UCEC_all_missense_variants_filtered.csv > non_hotspot_cases/UCEC_missense_variants.csv
grep -v -f UCEC_cases_with_hotspot_missense_variants.txt UCEC_all_missense_variants_filtered.csv >> non_hotspot_cases/UCEC_missense_variants.csv
```

```
cd hotspot_cases
Rscript ../../../scripts/tcga_trinuc_context_mutation_freqs.R
Rscript ../../../scripts/tcga_trinuc_context_plot.R
cd ../non_hotspot_cases
Rscript ../../../scripts/tcga_trinuc_context_mutation_freqs.R
```

### Compare tri nucleotide frequencies between different case sets 

```
prev = read.table("previous/UCEC_background_freqs.txt", header=F)
hotspot = read.table("hotspot_cases/UCEC_background_freqs.txt", header=F)
other = read.table("non_hotspot_cases/UCEC_background_freqs.txt", header=F)
x = prev
colnames(x) = c("context", "mutation", "bounds", "Ailith.hotspot.freq")
x$hotspot.freq = hotspot$V4
x$nonhotspot.freq = other$V4

panel.cor <- function(x, y, digits = 2, prefix = "", cex.cor, ...) {
     usr <- par("usr")
     on.exit(par(usr))
     par(usr = c(0, 1, 0, 1))
     Cor <- abs(cor(x, y)) # Remove abs function if desired
     txt <- paste0(prefix, format(c(Cor, 0.123456789), digits = digits)[1])
     if(missing(cex.cor)) {
         cex.cor <- 0.4 / strwidth(txt)
     }
     text(0.5, 0.5, txt,
          cex = 1 + cex.cor * Cor) # Resize the text by level of correlation
}

png("trinucleotide_context_mutation_freq_comparison.png", width=600, height=600)
pairs(x[,c(4,5,6)], upper.panel=panel.cor)
dev.off()
```

![Tri-nucleotide context mutation frequency comparison](../analysis/mutation_likelihood_scores/trinucleotide_context_mutation_freq_comparison.png "Tri-nucleotide context mutation frequency comparison")

![Tri-nucleotide context mutation plot (hotspot cases)](../analysis/mutation_likelihood_scores/hotspot_cases/trinucleotide_mutation_context.png)

### Calculate mutational likelihood scores

Script `codon2codon.py` generates all mutational paths from one codon to another (`*_full_paths.txt`), the per-path mutational likelihood score given background mutational frequencies from the given cancer type (`*_per_path_freqs.txt`), and the per-codon mutational likelihood score (summing over paths, `*_per_mutation_freqs.txt`).

`tnc_codon_summary.R` sums over codons to get a per-amino acid score, generated for all contexts, and for CTNNB1 hotspot contexts by position/reference AA only.

```
cd mutational_likelihood_scores/hotspot_cases
python3 ../../../scripts/codon2codon.py -b UCEC_background_freqs.txt -o UCEC -c ../../../data/resources/all_codons.txt
python3 ../../../scripts/codon2codon.py -b LIHC_background_freqs.txt -o LIHC -c ../../../data/resources/all_codons.txt
gzip *_full_paths.txt *_per_path_freqs.txt

Rscript ../../../scripts/tnc_codon_summary.R
```

