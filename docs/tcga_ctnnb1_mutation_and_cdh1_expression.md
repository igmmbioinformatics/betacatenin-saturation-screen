# Relationship between CTNNB1 mutation effect scores and CDH1 expression

From Andrew:

> Mechanistic explanations for the main finding from our CTNNB1 manuscript; i.e. that tumours originating in different tissue types favour CTNNB1 mutations of different effect size.

> One possible reason for this is that CTNNB1 has two functions in the cell - as a transcriptional co-factor (which is what our functional assay tests) and as a binding partner for E-Cadherin at Adherens junctions. Owen Sansom had an EMBO paper in 2015 (attached, but no obligation to read) suggesting that tissue-specific expression levels of E-Cadherin (CDH1) can act as a sponge, sequestering CTNNB1 away from the soluble pool capable of activating transcription. He showed that the large intestine has higher CDH1 expression compared to the small intestine, and is also harder to transform with the same activating CTNNB1 mutation (he used a deletion of the entire exon 3).

> If true, this hypothesis would predict that when we compare mutational effect scores of COSMIC mutations found in the large versus small intestine, they should be higher in the large intestine in order to overcome the greater effect of CDH1 sequestration.

> I tested this and it holds up (mean Small Intestine = 18123, n = 33 mutations, mean Large Intestine = 21366, n = 345 mutations, two-tailed t-test p = 0.0096).

> I would like to see if this correlation applies more broadly across tumours from other tissues for which we have CTNNB1 mutation data. Could you advise on the best way to obtain CDH1 expression data from the 15 cancer types from COSMIC that you have looked at? Would it be possible to correlate within individual tumours, rather than tumour types, where matched mutation and expression data exist? Simply stated, I want to test the hypothesis that tumours with higher CDH1 expression should have CTNNB1 mutations with higher mutational effect scores measured in our assay.

## Testing with TCGA data

### Obtaining data

1. In https://portal.gdc.cancer.gov/ repository, select cases with MODERATE or HIGH impact mutations in CTNNB1
2. Create a manifest to download all Mutect2 annotated somatic mutations in MAF format and all Gene level expression quantification files in TSV format
3. Download these files
4. Also download the JSON information about the files
5. Parse out the JSON info to get a table mapping case to MAF to TSV files

```
mkdir analysis/cdh1_expression
cd analysis/cdh1_expression
mkdir data
cd data
gdc-client download -t gdc-user-token.2022-11-01T09_28_52.783Z.txt -m gdc_manifest_20221121_100946.txt
gunzip */*.gz
ls */*.maf > ../maf_files.txt
ls */*.tsv > ../tsv_files.txt
cd ..
python ../../../scripts/parse_tcga_count_maf_files.py < files.2022-11-21.ctnnb1_mutations.maf_and_count.json > files.txt
```

Some of the files didn't download (non-TCGA projects, no permission).

1. Collect the ids of these files from the error output of `gdc-client download` and select these from the GDC Data Portal
2. Select the Mutect2 annotated somatic mutations in MAF format and all Gene level expression quantification files in TSV format
3. Download the JSON information about the files
4. Parse out the case ids into `cases.error_downloading.txt`
5. Remove these case ids from `files.txt`.

```
grep -v -f cases.error_downloading.txt files.txt > files_downloaded.txt
```

### Extracting CDH1 expression count data

Get just the CDH1 entries from the TSV files

```
cd data
grep ENSG00000039068 */*.tsv | sed -e 's/[:\/]/\t/g' | cut -f 2,6-11 > ../CDH1_expression.txt
```

### Extract the CTNNB1 missense mutations from the target regions from the MAF files

```
cd ..
Rscript ../../scripts/select_tcga_ctnnb1_target_mutations.R
```

### Map the CTNNB1 mutations to CDH1 expression via case id

Note - if there is more than one TSV file of expression data for a case, this script takes the mean value of the TPMs.

```
perl ../../scripts/merge_ctnnb1_mutation_cdh1_expression.pl tcga_ctnnb1_target_aachange.txt CDH1_expression.txt files_downloaded.txt > TCGA_CTNNB1_target_mutation_and_CDH1_expression.txt
```

### Move data out of analysis folder

```
mv data ../../data/ctnnb1_mutation_and_cdh1_expression
```

## Extract WNT target expression and map to CTNNB1 mutations

Mapped WNT targets to Ensembl ids with Biomart

```
ENSG00000188064 WNT7B
ENSG00000154764 WNT7A
ENSG00000065154 OAT
ENSG00000139292 LGR5
ENSG00000135111 TBX3
ENSG00000138795 LEF1
ENSG00000114251 WNT5A
ENSG00000135821 GLUL
ENSG00000168646 AXIN2
ENSG00000081059 TCF7
ENSG00000111432 FZD10
ENSG00000204335 SP5
```

Extracted expression 

```
cd data/ctnnb1_mutation_and_cdh1_expression
for ((i = 1; i <= 12; i = i + 1))
do
id=`head -n $i ../../analysis/cdh1_expression/WNT_targets.txt | tail -n 1 | cut -f 1`
name=`head -n $i ../../analysis/cdh1_expression/WNT_targets.txt | tail -n 1 | cut -f 2`
grep $id */*.tsv | sed -e 's/[:\/]/\t/g' | cut -f 2,6-11 > ../../analysis/cdh1_expression/${name}_expression.txt
done
```

```
cd analysis/cdh1_expression
perl ../../scripts/merge_ctnnb1_mutation_wnt_target_expression.pl tcga_ctnnb1_target_aachange.txt WNT_targets.txt files_downloaded.txt > TCGA_CTNNB1_target_mutation_and_WNT_target_expression.txt
```
