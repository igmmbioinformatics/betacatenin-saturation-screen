# This folder contains documentation sent from Andrew Wood via e-mail on 2021-05-31.

> I have attached a word document with details of the amplicons, sequence library details and file locations on the server. Martijn did a lot of work on this back in 2017/2018, and you will find all of his relevant files, including scripts, at this location: \\cmvm.datastore.ed.ac.uk\igmm\awood-lab\Former lab members data\Martijn\Betacatenin_saturation. 
> 
> His read counts and absolute codon frequencies are in the attached excel files, and he also included a description of his pipeline as a shell script. However this file (and the relevant thesis section that he refers to in his email below) was written for a different DMS project on GFP and not Ctnnb1. Nonetheless the general principles should hold. Martijn is happy to be contacted with questions but has a lot on his plate, and from past experience can be quite slow to respond.
> 
> I have also included the powerpoint presentation that I ran through with you last week at this location:
> \\cmvm.datastore.ed.ac.uk\igmm\awood-lab\Former lab members data\Martijn\Betacatenin_saturation\For_Alison_June2021

# Text from the file 'Details of amplicons and libraries'

## Notes for Alison on the Ctnnb1 Data analysis (AW 30/05/2021)

### Amplicon details
The region targeted for Deep mutational scanning spans codons 31 – 50 of mouse Ctnnb1. This is a single contiguous block within exon 3 (Mm10 Chr9:120,950,599-120,950,652)
Here is the wildtype sequence covered by the PCR amplicon spanning that region.

```
ATGGCCATGGAGCCGGACAGAAAAGCTGCTGTCAGCCACTGGCAGCAGCAGTCTTACTTGGATTCTGGAATCCATTCTGGTGCCACCACCACAGCTCCTTCCCTGAGTGGCAAGGGCAACCCTGAGGAAGAAGATGTTGACACCTCCCAAGTCCTTTATGAA
```

However, the repair templates introduced silent mutations at the 5’ end of this amplicon that were used to selectively prime the PCR amplifications on homology-directed-repair (HDR)-derived alleles. Here is the amplicon sequence with those changes included.

```
ATGGCCATGGAGCCGGACAGAAAAGCTGCTGTCAGCCACTGGCAGCAGCAGTCTTACTTGGATTCTGGAATCCATTCTGGTGCCACCACCACAGCTCCTTCCCTGAGTGGTAAAGGCAATCCCGAAGAAGAAGATGTTGACACCTCCCAAGTCCTTTATGAA
```

The raw, demultiplexed amplicon sequencing data are here:
`\\cmvm.datastore.ed.ac.uk\igmm\awood-lab\Former lab members data\Martijn\Betacatenin_saturation\_0_Raw_data_11061\all_reads`

### Sequence file details

There were 18 libraries from a single MiSeq run, comprising 2 biological replicate samples from each of 9 conditions labelled as follows:

* Plasmid: The homology-directed repair template plasmid library, that had never been transfected into cells. This tells us how well the repair template library was constructed.
* Pool: Amplified from cells that had been edited, but not sorted based on reporter activity. This sample serves as the baseline for allele frequencies in the total cell population, against which enrichment/depletion is calculated for each allele in samples P2-P7.
* P2/3/4/5/6/7: 6 different samples amplified from cells that had been edited, and then flow-sorted based on the level of GFP reporter activity. P2 = lowest, P7 = highest.
* Pool_HRM: Amplified from the same DNA template as ‘Pool’, but used a different reverse primer that did not span the region covered by the silent mutations introduced during HDR. This was included to estimate the HDR efficiency, but is not particularly important at this point.

### Repair template details
The saturation screen was done on an amino acid, rather than a nucleotide basis, so the library was supposed to comprise 19 positions x 20 codons (19 missense codons and 1 non-missense codon per position). Martijn found that several codons were absent or underrepresented in the plasmid library and (as would therefore be expected) all other libraries too.

It’s my understanding that only a single triplet was used to encode each amino acid in the repair template library, and the triplet was not chosen based on the WT codon identity. This means that some positions will look like they have synonymous mutations when you align HDR reads to the WT sequence, but there should be only 1 non-synonymous change per HDR read. 

Details of the codons used in the repair template library can be found in the Excel file ‘Codon details’ at `\\cmvm.datastore.ed.ac.uk\igmm\awood-lab\Former lab members data\Martijn\Betacatenin_saturation\For_Alison_June2021`

I confess that I am not 100% confident on the codon details for this last part, because I found a file sent by Derya in Peter Hohenstein’s lab that appeared to contradict some of the details. However I would suggest taking a look, seeing if things make sense and then we can pick Martijn’s brain if necessary. 


