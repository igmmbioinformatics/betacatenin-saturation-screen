# Input data and resources

## Raw reads from beta catenin saturation screen

There were 18 libraries from a single MiSeq run, comprising 2 biological replicate samples from each of 9 conditions labelled as follows:

* Plasmid: The homology-directed repair template plasmid library, that had never been transfected into cells. This tells us how well the repair template library was constructed.
* Pool: Amplified from cells that had been edited, but not sorted based on reporter activity. This sample serves as the baseline for allele frequencies in the total cell population, against which enrichment/depletion is calculated for each allele in samples P2-P7.
* P2/3/4/5/6/7: 6 different samples amplified from cells that had been edited, and then flow-sorted based on the level of GFP reporter activity. P2 = lowest, P7 = highest.
* Pool_HRM: Amplified from the same DNA template as ‘Pool’, but used a different reverse primer that did not span the region covered by the silent mutations introduced during HDR. This was included to estimate the HDR efficiency, but is not particularly important at this point.

```
rsync -av /exports/igmm/datastore/awood-lab/Former lab members data/Martijn/Betacatenin_saturation/_0_Raw_data_11061/all_reads/* /exports/igmm/eddie/bioinfsvice/ameynert/betacanenin_saturation_screen/data/reads/
```

## Reference sequences

162bp sequence

### Wild-type Ctnnb1

[ctnnb1_wt.fa](../data/resources/ctnnb1_wt.fa)

```
ATGGCCATGGAGCCGGACAGAAAAGCTGCTGTCAGCCACTGGCAGCAGCAGTCTTACTTGGATTCTGGAATCCATTCTGGTGCCACCACCACAGCTCCTTCCCTGAGTGGCAAGGGCAACCCTGAGGAAGAAGATGTTGACACCTCCCAAGTCCTTTATGAA
```

### Ctnnb1 with silent changes for HDR priming

[ctnnb1_ref.fa](../data/resources/ctnnb1_ref.fa)

```
ATGGCCATGGAGCCGGACAGAAAAGCTGCTGTCAGCCACTGGCAGCAGCAGTCTTACTTGGATTCTGGAATCCATTCTGGTGCCACCACCACAGCTCCTTCCCTGAGTGGTAAAGGCAATCCCGAAGAAGAAGATGTTGACACCTCCCAAGTCCTTTATGAA
```

### Differences between WT and reference sequence

wt pos ref

* C111T
* G114A
* C120T
* T123C
* G126A

### Sequence of interest - codons to be changed

```
TTGGATTCTGGAATCCATTCTGGTGCCACCACCACAGCTCCTTCCCTGAGTGGTAAAGGC
```

# Codon changes

[codons.csv](data/resources/codons.csv)

```
M	ATG
N	AAC
I	ATC
L	CTG
A	GCT
F	TTC
Q	CAG
G	GGT
D	GAT
K	AAG
T	ACC
W	TGG
R	AGA
S	AGC
C	TGC
V	GTG
P	CCT
H	CAC
E	GAG
Y	TAC
```

# Codon sequence

Region of interest starts at codon/aa position 31 and runs to position 50. File includes codon sequences.

[codons.csv](data/resources/reference_aa_pos.csv)

```
19 K
20 A
21 A
22 V
23 S
24 H
25 W
26 Q
27 Q
28 Q
29 S
30 Y
31 L
32 D
33 S
34 G
35 I
36 H
37 S
38 G
39 A
40 T
41 T
42 T
43 A
44 P
45 S
46 L
47 S
48 G
49 K
50 G
```
