# Correlation between replicate samples

Working folder: analysis/replicate_correlation

Mutation file format
```
  codon_pos nt_pos codon aa count        freq
1         1     58   AAC  N   897 0.001646672
2         1     58   AAG  K  1709 0.003137305
3         1     58   ACC  T  1481 0.002718753
4         1     58   AGA  R  1904 0.003495278
5         1     58   AGC  S   883 0.001620972
6         1     58   ATC  I  1707 0.003133634
```

Scripts:

* [replicate_correlation.R](../scripts/replicate_correlation.R)

Output:

* [replicate_correlation.csv](../analysis/replicate_correlation/replicate_correlation.csv)

```
Rscript ../../replicate_correlation.R
```

## Replication excluding positions 19 & 20

Scripts:

* [replicate_correlation_excl_19_20.R](../scripts/replicate_correlation_excl_19_20.R)

Output:

* [replicate_correlation_excluding_codons_19_20.csv](../analysis/replicate_correlation/replicate_correlation_excluding_codons_19_20.csv)

```
Rscript ../../replicate_correlation_excl_19_20.R
```
