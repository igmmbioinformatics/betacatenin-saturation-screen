# Read processing

Aim: go from raw reads to read/mutation pairs. The Nextflow pipeline will run 4 steps:
* Trim adapters with [NGmerge in adapter mode](https://github.com/harvardinformatics/NGmerge#adapter)
* Merge read 1 & read 2 with [NGmerge in stitch mode](https://github.com/harvardinformatics/NGmerge#stitch)
* Align the new single end reads against the reference sequence
* For each read, identify differences from the reference genome that match the expected codons from the mutagenesis screen
  * Exclude reads with indels
  * Exclude reads with unexpected codons
  * Exclude reads with multiple mutated codons even if they match the expected sequence

## Output files

* read_merging
  * log file of trimming adaptors by intersecting read pairs (`${sample}_adaptor_removal.log`)
  * log file of merging trimmed read pairs into a single read (`${sample}_stitch_reads.log`)
  * reads that failed merging (`${sample}_failed_merge_[1/2].fast.gz`)
  * merged reads (`${sample}_merged.fastq.gz`)
* alignment
  * reads aligned against Ctnnb1_ref.fa (`${sample}.bam` and index file `${sample}.bam.bai`)
  * basic alignment statistics (`${sample}.bam.stats`)
* mutations
  * reads that did not fully cover the target region within the reference sequence (`${sample}_reads_not_fully_covering_region.log` - read name only)
  * reads with no mutations in the target region (`${sample}_reads_with_no_mutations.log` - read name only)
  * reads with mutations outside the target region (`${sample}_reads_with_offtarget_mutations.log` - read name, reference position, nucleotide mutation)
  * reads with unexpected codons (not those that should have been edited in via CRISPR) (`${sample}_reads_with_unexpected_codons.log` - read name, reference nt position, observed codon)
  * reads with multiple codons (expected sequence) in target region (`${sample}_reads_with_multiple_codons.log` - read name only)
  * counts of observed codons in reads with a single expected codon in the target region (`${sample}_mutations.txt` - has header: codon position, reference nt position, codon, amino acid, count) - note that some of the reads counted here may also have off-target nucleotide mutations

## Prepare input

In: /exports/igmm/eddie/bioinfsvice/ameynert/betacatenin_saturation_screen/analysis/read_processing

```
ls ../../data/reads/*/*_1.fastq.gz > read1.txt
ls ../../data/reads/*/*_2.fastq.gz > read2.txt
ls ../../data/reads/ | cut -d '/' -f 5 > samples.txt
paste -d ',' samples.txt read1.txt read2.txt > input.csv
```

## Run Nextflow pipeline

```
nextflow run ../../betacatenin-saturation-screen/pipelines/read_processing/main.nf --input input.csv \
  --reference ../../data/resources/ctnnb1_ref.fa --codons ../../data/resources/codons.txt
```

# Read metrics

[read_metrics.csv](../analysis/read_processing/read_metrics.csv)

## Read pairs

```
awk '{ print $1 "\t" $8}' analysis/qc/fastqc_multiqc_report_data/multiqc_general_stats.txt | uniq

Sample	
Plasmid_1	668139.0
Plasmid_2	604564.0
Pool_1HRM	659425.0
Pool_2HRM	586746.0
p2_1	719553.0
p2_2	867294.0
p3_1	744119.0
p3_2	689460.0
p4_1	739290.0
p4_2	740155.0
p5_1	621277.0
p5_2	703564.0
p6_1	648641.0
p6_2	611360.0
p7_1	590626.0
p7_2	588312.0
pool_1	800629.0
pool_2	748001.0
```

## Reads failing merge

```
for file in analysis/read_processing/output/read_merging/*1.fastq.gz
do
  echo `basename ${file%_failed_merge_1.fastq.gz}` `zcat $file | awk 'NR % 4 == 0' | wc -l`
done

p2_1 11738
p2_2 17340
p3_1 11834
p3_2 10720
p4_1 11467
p4_2 12833
p5_1 8403
p5_2 8908
p6_1 7128
p6_2 7430
p7_1 6310
p7_2 6216
Plasmid_1 9606
Plasmid_2 11555
pool_1 13411
Pool_1HRM 555359
pool_2 14426
Pool_2HRM 516377
```

## Aligner input (reads passing merge)

```
for file in analysis/read_processing/output/read_merging/*adapter_removal.log
do
  echo `basename ${file%_adapter_removal.log}` `grep -cv Adapter $file`
done

p2_1 707815
p2_2 849954
p3_1 732285
p3_2 678740
p4_1 727823
p4_2 727322
p5_1 612874
p5_2 694656
p6_1 641510
p6_2 603929
p7_1 584316
p7_2 582096
Plasmid_1 658532
Plasmid_2 593007
pool_1 787217
Pool_1HRM 104065
pool_2 733575
Pool_2HRM 70369
```

## Aligned reads

```
cd analysis/read_processing/output/alignments/
grep 'reads mapped:' *.stats | cut -d ':' -f 1,3 | sed -e 's/\.bam\.stats://'

p2_1	707814
p2_2	849954
p3_1	732132
p3_2	678739
p4_1	727821
p4_2	727318
p5_1	612874
p5_2	694655
p6_1	641512
p6_2	603930
p7_1	584316
p7_2	582095
Plasmid_1	658533
Plasmid_2	593008
pool_1	787218
Pool_1HRM	104065
pool_2	733575
Pool_2HRM	70369
```

## Reads mapped without indels

```
cd analysis/read_processing/output/alignments/
for file in *.bam
do
  echo ${file%.bam} `samtools view $file | grep -c 102M`
done

p2_1 618631
p2_2 745379
p3_1 659753
p3_2 613036
p4_1 668775
p4_2 666172
p5_1 590138
p5_2 661953
p6_1 622284
p6_2 585811
p7_1 563829
p7_2 566890
Plasmid_1 596629
Plasmid_2 540953
pool_1 713838
Pool_1HRM 98775
pool_2 656845
Pool_2HRM 66968
```

## Reads not fully covering region of interest

```
cd analysis/read_processing/output/mutations
cat *.reads_not_fully_covering_region.log | wc -l
0
```

## Reads with unexpected codons

```
for file in *.reads_with_unexpected_codons.log
do
  echo ${file%.reads_with_unexpected_codons.log} `cut -f 1 $file | sort -u | wc -l`
done

p2_1 88660
p2_2 126779
p3_1 103896
p3_2 95266
p4_1 104451
p4_2 111639
p5_1 87888
p5_2 95322
p6_1 83945
p6_2 82216
p7_1 69457
p7_2 73981
Plasmid_1 77710
Plasmid_2 82607
Pool_1HRM 36993
pool_1 109483
Pool_2HRM 27080
pool_2 109376
```

## Reads with multiple mutated codons

```
wc -l *.reads_with_multiple_codons.log | sed -e 's/\.reads_with_multiple_codons\.log//'
   19382 p2_1
   30106 p2_2
   24604 p3_1
   23242 p3_2
   24565 p4_1
   25748 p4_2
   22380 p5_1
   23238 p5_2
   22284 p6_1
   22697 p6_2
   22117 p7_1
   17380 p7_2
   15728 Plasmid_1
   16335 Plasmid_2
    5574 Pool_1HRM
   24970 pool_1
    4050 Pool_2HRM
   25105 pool_2
```

## Read without any mutations

```
wc -l *.reads_with_no_mutations.log | sed -e 's/\.reads_with_no_mutations\.log//'
   40315 p2_1
   40716 p2_2
   34690 p3_1
   25289 p3_2
   29685 p4_1
   29485 p4_2
   14032 p5_1
   13749 p5_2
    9308 p6_1
    9549 p6_2
    4480 p7_1
    6053 p7_2
   22686 Plasmid_1
   25764 Plasmid_2
   15996 Pool_1HRM
   34650 pool_1
   11382 Pool_2HRM
   31161 pool_2
```


## Reads with one mutated codon of the expected sequence

```
for file in *.mutations.txt
do
  echo ${file%.mutations.txt} `grep -v codon $file | awk '{ sum += $4 } ; END { print sum }'`
done

p2_1 470274
p2_2 547778
p3_1 496563
p3_2 469239
p4_1 510074
p4_2 499300
p5_1 465838
p5_2 529644
p6_1 506747
p6_2 471349
p7_1 467775
p7_2 469476
Plasmid_1 480505
Plasmid_2 416247
Pool_1HRM 40212
pool_1 544735
Pool_2HRM 24456
pool_2 491203
```

## Sanity check

In Excel, confirmed that reads with 1 expected mutation + reads with > 1 expected mutation + reads with >= 1 unexpected mutation + unmutated reads = total reads without indels.

# Codon usage check

## How frequently is each target codon used?

```
R
x = data.frame(); for (f in list.files(pattern = ".mutations.txt")) { y = read.table(f, header=T); x = rbind(x, y); }

aggregate(x$count, by = list(x$codon, x$aa), sum)
   Group.1 Group.2      x
1      GCT       A 363706
2      TGC       C 377315
3      GAT       D 413900
4      GAG       E 474417
5      TTC       F 402833
6      GGT       G 366852
7      CAC       H 386299
8      ATC       I 382790
9      AAG       K 426354
10     CTG       L 383543
11     ATG       M 426519
12     AAC       N 397653
13     CCT       P 412421
14     CAG       Q 454517
15     AGA       R 435488
16     AGC       S 288952
17     ACC       T 287697
18     GTG       V 393068
19     TGG       W 423787
20     TAC       Y 403304

y = aggregate(x$count, by = list(x$codon, x$aa), sum)
summary(y)
    Group.1      Group.2         x         
 AAC    : 1   A      : 1   Min.   :287697  
 AAG    : 1   C      : 1   1st Qu.:381421  
 ACC    : 1   D      : 1   Median :400243  
 AGA    : 1   E      : 1   Mean   :395071  
 AGC    : 1   F      : 1   3rd Qu.:424429  
 ATC    : 1   G      : 1   Max.   :474417  
 (Other):14   (Other):14                   

sd(y$x)
[1] 46052.63

library(lattice)
png("codon_usage.png", width=600, height=600)
histogram(~x, y, xlab="Count", main="Codon usage across all samples", breaks=seq(275000,475000,25000), type="c")
dev.off()
```

The codons that are used least across all samples are ACC for T (287,697) and AGC for S (288,952). All others are in the range 350,000-475,000.

![Codon usage](../analysis/read_processing/output/mutations/codon_usage.png)

## Check the unexpected codons

None of the unexpected codons comes close to even 50% the usage of the 20 that were specified.

```
cat *.reads_with_unexpected_codons.log | cut -f 3 | sort | uniq -c | sort > unexpected_codon_usage.txt

R
unexpected = read.table("unexpected_codon_usage.txt", col.names=c("count", "codon"), stringsAsFactors=F)
unexpected$type = 'unexpected'
expected = data.frame(count = y$x, codon = as.character(y$Group.1), aa = as.character(y$Group.2))
expected$type = 'expected'

all = read.table("../../../../data/resources/all_codons.txt", header=F, stringsAsFactors=F)
for(codon in unexpected$codon) { unexpected$aa[unexpected$codon == codon] = all$V3[all$V1 == codon] }

combined = rbind(expected, unexpected)
combined.sorted = combined[order(combined$aa,combined$count),]
write.table(combined.sorted, "all_codon_usage.txt", col.names=T, row.names=F, quote=F, sep="\t")
```

It's clear that the expected codon for each amino acid is the most observed by a long way. I've flagged the two highest (>100K seen) unexpected codons.

```
count	codon	aa	type

31534	GCG	A	unexpected
37295	GCA	A	unexpected
59257	GCC	A	unexpected
363706	GCT	A	expected

115749	TGT	C	unexpected <--
377315	TGC	C	expected

52496	GAC	D	unexpected
413900	GAT	D	expected

15419	GAA	E	unexpected
474417	GAG	E	expected

54295	TTT	F	unexpected
402833	TTC	F	expected

28435	GGA	G	unexpected
52092	GGG	G	unexpected
70418	GGC	G	unexpected
366852	GGT	G	expected

60054	CAT	H	unexpected
386299	CAC	H	expected

15826	ATT	I	unexpected
41506	ATA	I	unexpected
382790	ATC	I	expected

12422	AAA	K	unexpected
426354	AAG	K	expected

5490	TTA	L	unexpected
8565	CTA	L	unexpected
12979	TTG	L	unexpected
15782	CTC	L	unexpected
60732	CTT	L	unexpected
383543	CTG	L	expected

426519	ATG	M	expected

57408	AAT	N	unexpected
397653	AAC	N	expected

11795	CCA	P	unexpected
22643	CCG	P	unexpected
88566	CCC	P	unexpected
412421	CCT	P	expected

7382	CAA	Q	unexpected
454517	CAG	Q	expected

4259	CGA	R	unexpected
8641	CGC	R	unexpected
13652	CGG	R	unexpected
16799	CGT	R	unexpected
20561	AGG	R	unexpected
435488	AGA	R	expected

18814	AGT	S	unexpected
22425	TCG	S	unexpected
48044	TCT	S	unexpected
52378	TCA	S	unexpected
88934	TCC	S	unexpected
288952	AGC	S	expected

25456	ACG	T	unexpected
66684	ACA	T	unexpected
73414	ACT	T	unexpected
287697	ACC	T	expected

36679	GTT	V	unexpected
45733	GTA	V	unexpected
45751	GTC	V	unexpected
393068	GTG	V	expected

423787	TGG	W	expected

101398	TAT	Y	unexpected <--
403304	TAC	Y	expected

5524	TAA	O	unexpected
14353	TAG	O	unexpected
66322	TGA	O	unexpected
```
