# Normalizing pools p2 through p7 against pool samples

Working folder: analysis/normalized_pools

Scripts:
* [normalized_pools.R](../../scripts/normalized_pools.R)
* [normalized_pools_plot_unnormalized_freqs.R](../../scripts/normalized_pools_plot_unnormalized_freqs.R)
* [normalized_pools_plot_normalized_freqs.R](../../scripts/normalized_pools_plot_normalized_freqs.R)

```
Rscript ../../scripts/normalized_pools.R
Rscript ../../scripts/normalized_pools_plot_unnormalized_freqs.R
Rscript ../../scripts/normalized_pools_plot_normalized_freqs.R
```

## Plots

### Unnormalized frequencies

![Population frequencies by codon position](../analysis/normalized_pools/population_freq_by_codon_and_position.png "Population frequencies by codon position")

### Frequencies normalized against pool

![Population frequencies normalized against pool frequencies by codon position](../analysis/normalized_pools/population_norm_freq_by_codon_and_position.png "Population frequencies normalized against pool frequencies by codon position")
