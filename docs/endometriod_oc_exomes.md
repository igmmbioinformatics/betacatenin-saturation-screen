# Endometriod ovarian cancer exomes

* From Charlie Gourley's group, [EGAD00001006389](https://ega-archive.org/datasets/EGAD00001006389).
* [CTNNB1 coordinates](http://May2021.archive.ensembl.org/Homo_sapiens/Gene/Summary?db=core;g=ENSG00000168036;r=3:41194741-41260096)

## Scripts

* [endometriod_oc_exomes_copy_data.sh](../scripts/endometriod_oc_exomes_copy_data.sh)
* [endometriod_oc_exomes_extract_missense.sh](../scripts/endometriod_oc_exomes_extract_missense.sh)
* [endometriod_oc_exomes_select_missense_31-48.R](../scripts/endometriod_oc_exomes_select_missense_31-48.R)
* [endometriod_oc_exomes_map_3AA_codes_to_1AA_codes.R](../scripts/endometriod_oc_exomes_map_3AA_codes_to_1AA_codes.R)
* [endometriod_oc_exomes_plot_missense.R](../scripts/endometriod_oc_exomes_plot_missense.R)

## Variant summary

Start in base folder

```
./scripts/endometriod_oc_exomes_copy_data.sh
```

* [endometriod_oc_sample_ids.txt](../analysis/endometriod_oc_exome/endometriod_oc_sample_ids.txt)
* [variant_summary.txt](../analysis/endometriod_oc_exome/variant_summary.txt)

## Extract relevant info, subset to missense, and plot

In analysis/endometriod_ovarian

```
../../scripts/endometriod_oc_exomes_extract_missense.sh
```

* [variants.CTNNB1.txt](../analysis/endometriod_oc_exome/variants.CTNNB1.txt)
* [variants.CTNNB1.missense.intarget.csv](../analysis/endometriod_oc_exome/variants.CTNNB1.missense.intarget.csv)

![Endometriod ovarian carcinoma mutation counts](../analysis/endometriod_oc_exome/endometriod_ovarian_missense_mutations.png "Endometriod ovarian carcinoma mutation counts")
