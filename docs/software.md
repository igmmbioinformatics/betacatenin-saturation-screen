# Software used

## MultiQC

Version 1.10.1

```
conda create -n multiqc
conda activate multiqc
conda install multiqc
```

## R packages

```
module load igmm/apps/R/4.1.0
```

* dplyr
* tidyr
* ggplot2
* reshape2
* RColorBrewer
* maftools
