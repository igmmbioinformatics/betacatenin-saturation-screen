# Read quality control

FastQC version v0.11.8

```
module load igmm/apps/bcbio/1.2.0
fastqc -version
FastQC version v0.11.8
fastqc */*.gz

conda activate multiqc
multiqc -n ../../analysis/qc/fastqc_multiqc_report.html --dirs .
```

[FastQC MultiQC report](../analysis/qc/fastqc_multiqc_report.html)

