# Checking library representation in pool vs plasmid sample

Plasmid sample = representation of codons in library
Pool sample = representation of codons after editing without selection

Sanity check of pool vs plasmid proportions to check if there was any significant drop-out of codons at any position.

Working folder: analysis/pool\_vs\_plasmid

Scripts:

* [pool_vs_plasmid_library_representation.R](../scripts/pool_vs_plasmid_library_representation.R)

```
Rscript ../../scripts/pool_vs_plasmid_library_representation.R
```

![Dotplot of plasmid vs pool codon frequencies by position](../analysis/pool_vs_plasmid/plasmid_vs_pool_dotplot.png "Dotplot of plasmid vs pool codon frequencies by position")

Correlation between pool and plasmid frequencies

```
[1] 0.7196174
```

Linear model of pool ~ Plasmid

```
Call:
lm(formula = pool ~ Plasmid, data = res2)

Coefficients:
(Intercept)      Plasmid  
 -0.0002906    1.1063537  
```

Position/codon pairs where frequency in pool is noteably higher than in the plasmid - these are all at codon position 19 (of 20).

```
    codon_pos nt_pos codon aa     Plasmid        pool
355        19    112   AGA  R 0.004173952 0.005196257
356        19    112   AGC  S 0.003781425 0.006908715
358        19    112   ATG  M 0.004216327 0.007058337
359        19    112   CAC  H 0.003382206 0.006227207
360        19    112   CAG  Q 0.003366594 0.005092004
361        19    112   GAG  E 0.004593243 0.009838427
362        19    112   GAT  D 0.003950925 0.007367236
363        19    112   TAC  Y 0.003081119 0.006408685
```

Position/codon pairs where frequency in both pool & plasmid is low - reasonably scattered across positions, bias towards S (4 occurrences) and G (2 occurrences). No real difference b/w pool & plasmid.

```
    codon_pos nt_pos codon aa      Plasmid         pool
11          1     58   CTG  L 8.586543e-05 1.032880e-04
44          3     64   AGC  S 1.115136e-05 1.351432e-05
74          4     67   GGT  G 2.163363e-04 1.882352e-04
106         6     73   CAC  H 1.538887e-04 1.814780e-04
123         7     76   AGC  S 2.341785e-05 8.108593e-05
171         9     82   GCT  A 1.327011e-04 2.133332e-04
218        12     91   ACC  T 3.769158e-04 4.797584e-04
278        15    100   AGC  S 4.572056e-05 4.923075e-05
317        17    106   AGC  S 4.427088e-04 4.257012e-04
336        18    109   AGA  R 1.450791e-03 9.257311e-04
353        19    112   AAG  K 3.412315e-04 5.560178e-04
365        20    115   GGT  G 1.483130e-04 6.564099e-05
```

## Re-run analysis dropping last two amino acid positions

Positions 19 & 20 of the target region were not well represented in the screen. It was decided to remove these from subsequent analysis.

Scripts:

* [pool_vs_plasmid_library_representation_excl_pos_19_20.R](../scripts/pool_vs_plasmid_library_representation_excl_pos_19_20.R)

```
Rscript ../../scripts/pool_vs_plasmid_library_representation_excl_pos_19_20.R
```

![Dotplot of plasmid vs pool codon frequencies by position, excluding 19 & 20](../analysis/pool_vs_plasmid/plasmid_vs_pool_excluding_19_20_dotplot.png "Dotplot of plasmid vs pool codon frequencies by position excluding positions 19 & 20")

Correlation between pool and plasmid frequencies

```
[1] 0.731726
```

Linear model of pool ~ Plasmid

```
Call:
lm(formula = pool ~ Plasmid, data = res2)

Coefficients:
(Intercept)      Plasmid  
  0.0001351    0.9525757   
```

Position/codon pairs where frequency in both pool & plasmid is low - reasonably scattered across positions, bias towards S (4 occurrences). No real difference b/w pool & plasmid. **Note:** all but one of these are synonymous substitutions, which should not have been targeted by the screen. None of the other possible synonymous substitutions are observed. 

```
[1] "Position/codon pairs where frequency in both plasmid and pool is low"
    codon_pos nt_pos codon aa      Plasmid         pool ref_codon_pos ref_aa  ref_codon
11          1     58   CTG  L 9.012404e-05 1.113298e-04            31      L        TTG
44          3     64   AGC  S 1.170442e-05 1.456652e-05            33      S        TCT
74          4     67   GGT  G 2.270658e-04 2.028908e-04            34      G        GGA
106         6     73   CAC  H 1.615210e-04 1.956076e-04            36      H        CAT
123         7     76   AGC  S 2.457928e-05 8.739913e-05            37      S        TCT
171         9     82   GCT  A 1.392826e-04 2.299429e-04            39      A        GCC
218        12     91   ACC  T 3.956094e-04 5.171115e-04            42      T        ACA
219        12     91   AGA  R 1.326111e-03 1.483704e-03            42      T        ACA
278        15    100   AGC  S 4.798813e-05 5.306376e-05            45      S        TCC
317        17    106   AGC  S 4.646655e-04 4.588454e-04            47      S        AGT


[1] "Positions with synonymous substitutions"
    codon_pos nt_pos codon aa      Plasmid         pool ref_codon_pos ref_aa  ref_codon
11          1     58   CTG  L 9.012404e-05 1.113298e-04            31      L        TTG
44          3     64   AGC  S 1.170442e-05 1.456652e-05            33      S        TCT
74          4     67   GGT  G 2.270658e-04 2.028908e-04            34      G        GGA
106         6     73   CAC  H 1.615210e-04 1.956076e-04            36      H        CAT
123         7     76   AGC  S 2.457928e-05 8.739913e-05            37      S        TCT
171         9     82   GCT  A 1.392826e-04 2.299429e-04            39      A        GCC
218        12     91   ACC  T 3.956094e-04 5.171115e-04            42      T        ACA
278        15    100   AGC  S 4.798813e-05 5.306376e-05            45      S        TCC
317        17    106   AGC  S 4.646655e-04 4.588454e-04            47      S        AGT
```

It appears that synonymous changes were not included in the screen, probably due to ~half of the codons in the reference sequence matching the target codons for mutagenesis.

```
AA pos Ref AA  Ref codon Target codon Codon identity Plasmid freq Pool freq
    31      L        TTG          CTG              0     9.01E-05  1.11E-04
    32      D        GAT          GAT              1           
    33      S        TCT          AGC              0     1.17E-05  1.46E-05
    34      G        GGA          GGT              0     2.27E-04  2.03E-04
    35      I        ATC          ATC              1           
    36      H        CAT          CAC              0     1.62E-04  1.96E-04
    37      S        TCT          AGC              0     2.46E-05  8.74E-05
    38      G        GGT          GGT              1           
    39      A        GCC          GCT              0     1.39E-04  2.30E-04
    40      T        ACC          ACC              1           
    41      T        ACC          ACC              1           
    42      T        ACA          ACC              0     3.96E-04  5.17E-04
    43      A        GCT          GCT              1           
    44      P        CCT          CCT              1           
    45      S        TCC          AGC              0     4.80E-05  5.31E-05
    46      L        CTG          CTG              1           
    47      S        AGT          AGC              0     4.65E-04  4.59E-04
    48      G        GGT          GGT              1
```

Checking counts of synonymous changes.

```
  Group.1    x
1 Plasmid 1335
2    pool 1798
  Group.1      x
1 Plasmid 854378
2    pool 961108
[1] "Proportion of synonymous mutations"
[1] 0.001562540 0.001870758
```

## Re-run analysis dropping last two amino acid positions & synonymous changes

Scripts:

* [pool_vs_plasmid_library_representation_excl_pos_19_20_syn.R](../scripts/pool_vs_plasmid_library_representation_excl_pos_19_20_syn.R)

```
Rscript ../../scripts/pool_vs_plasmid_library_representation_excl_pos_19_20_syn.R
```

![Dotplot of plasmid vs pool codon frequencies by position, excluding positions 19 & 20 and synonymous changes](../analysis/pool_vs_plasmid/plasmid_vs_pool_excluding_19_20_synonymous_dotplot.png "Dotplot of plasmid vs pool codon frequencies by position excluding positions 19 & 20 and synonymous changes")

Correlation between pool and plasmid frequencies

```
[1] 0.626078
```

Linear model of pool ~ Plasmid

```
Call:
lm(formula = pool ~ Plasmid, data = res2)

Coefficients:
(Intercept)      Plasmid  
   0.000209     0.928523  
```

## Plots of plasmid and pool frequency as barcharts

Scripts:

* [pool_vs_plasmid_freqs.R](../scripts/pool_vs_plasmid_freqs.R)

```
Rscript ../../scripts/pool_vs_plasmid_freqs.R
```

![Plasmid frequencies by codon position](../analysis/pool_vs_plasmid/plasmid_freq_by_codon_and_position.png "Plasmid frequencies by codon position")

![Pool frequencies by codon position](../analysis/pool_vs_plasmid/pool_freq_by_codon_and_position.png "Pool frequencies by codon position")
