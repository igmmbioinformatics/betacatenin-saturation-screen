# Analysis of COSMIC mutation data

## Data acquisition

Targeted and genome-wide mutations were downloaded from https://cancer.sanger.ac.uk/cosmic/download, filtered by gene CTNNB1, from COSMIC release v94 (28 May 2021). The file V94_38_MUTANT.csv was copied to data/resources and contains 9248 mutations across 8622 samples (using SAMPLE_NAME).

## Mutation summaries

Script: [cosmic_mutation_summary.R](../../scripts/cosmic_mutation_summary.R)

In: analysis/cosmic

```
Rscript ../../scripts/cosmic_mutation_summary.R
```

* [mutation_counts_by_type.csv](../../analysis/cosmic/mutation_counts_by_type.csv) - across all of CTNNB1
* cosmic.missense.csv - too large for gitlab, selects for 'Substitution - Missense' in MUTATION_DESCRIPTION and excludes one incorrectly assigned indel `p.I35_H36delinsSN`
* cosmic.missense.target.csv - too large, includes only missense mutations in the target region of AA 31 to 48
* [cosmic.missense.target.zygosity.csv](../../analysis/cosmic/cosmic.missense.target.zygosity.csv) - based on cosmic.missense.target.csv, sums over aa ref, pos, aa alt, zygosity
* [mutation_counts_by_primary_site.csv](../../analysis/cosmic/mutation_counts_by_primary_site.csv) - based on cosmic.missense.target.csv
* [sample_counts_by_primary_site.csv](../../analysis/cosmic/sample_counts_by_primary_site.csv) - based on cosmic.missense.target.csv
* [mutation_counts_by_aa_pos.csv](../../analysis/cosmic/mutation_counts_by_aa_pos.csv) - based on cosmic.missense.target.csv

## Plot mutation counts overall & for selected primary sites

Use primary sites with at least 50 samples

* [sample_counts_50plus_by_primary_site.csv](../../analysis/cosmic/sample_counts_50plus_by_primary_site.csv) - based on cosmic.missense.target.csv

Script: [cosmic_plot_missense_counts.R](../../scripts/cosmic_plot_missense_counts.R)

In: analysis/cosmic

```
Rscript ../../scripts/cosmic_plot_missense_counts.R
```

![COSMIC v94 mutation counts](../analysis/cosmic/cosmic_missense_mutations.png "COSMIC v94 mutation counts")

![COSMIC v94 mutation counts by primary site](../analysis/cosmic/cosmic_missense_counts_by_primary_sites.png "COSMIC v94 mutation counts by primary site")

## Heatmap and cluster of selected primary sites by mutation position frequency and by mutation frequency

Using primary sites with at least 50 samples as above.

Script: [cosmic_plot_top50_primary_sites_heatmap.R](cosmic_plot_top50_primary_sites_heatmap.R)

In: analysis/cosmic

```
Rscript ../../scripts/cosmic_plot_top50_primary_sites_heatmap.R
```

![COSMIC v94 mutation position frequency clustering by primary site](../analysis/cosmic/cosmic_top50_primary_sites_heatmap.png)

![COSMIC v94 mutation frequency clustering by primary site](../analysis/cosmic/cosmic_top50_primary_sites_by_aachange_heatmap.png)

## Lollipop plot of CTNNB1

Scripts:
* [cosmic_to_maf_lollipop.pl](../scripts/cosmic_to_maf_lollipop.pl)
* [cosmic_ctnnb1_lollipop_plot.R](../scripts/cosmic_ctnnb1_lollipop_plot.R)

In: analysis/cosmic

```
perl ../../scripts/cosmic_to_maf_lollipop.pl \
  --input ../../data/resources/V94_38_MUTANT.csv \
  --header ../../data/resourcesmaf_header.txt \
  --aamap ../../data/resourcesall_codons.txt \
  --output cosmic.lollipop.maf

Rscript ../../scripts/cosmic_ctnnb1_lollipop_plot.R
```

![COSMIC v94 CTNNB1 mutations](../analysis/cosmic/cosmic_ctnnb1_lollipop.png)

Alternative plot for paper

```
Rscript ../../scripts/alternate_cosmic_lollipop_plot.R
```

![COSMIC v94 CTNNB1 substitution mutations](../analysis/cosmic/cosmic_substitution_lollipop.png)

## COSMIC samples with CNVs and point mutations

* Download COSMIC samples from https://cancer.sanger.ac.uk/cosmic/download 2022-11-09
* Download COSMIC Ctnnb1 CNVs from https://cancer.sanger.ac.uk/cosmic/gene/analysis?all_data=y&coords=AA%3AAA&dr=&end=782&gd=&id=357810&ln=CTNNB1&seqlen=782&src=gene&start=1#cnv_t 2022-11-09

Scripts:
* [cosmic_cnvs_with_mutations.R](../scripts/cosmic_cnvs_with_mutations.R)

Files:
* [cosmic.cnvs.with.missense.target.mutations.csv](../../analysis/cosmic/cosmic.cnvs.with.missense.target.mutations.csv)

```
grep -v sample_name cosmic_samples_20221109.tsv | cut -f 1,2 | grep TCGA > cosmic_tcga_sample_id_map.txt
Rscript ../../scripts/cosmic_cnvs_with_mutations.R
```

Only 34 samples with CTNNB1 CNVs and missense mutations in the target region in CTNNB1 - 4 with S45 mutations.

## TCGA exome cases in same primary sites as COSMIC samples with CTNNB1 missense mutations in target region

Files:

Downloaded case counts for [TCGA cases with exome sequencing](https://portal.gdc.cancer.gov/repository?facetTab=cases&filters=%7B%22op%22%3A%22and%22%2C%22content%22%3A%5B%7B%22op%22%3A%22in%22%2C%22content%22%3A%7B%22field%22%3A%22cases.project.program.name%22%2C%22value%22%3A%5B%22TCGA%22%5D%7D%7D%2C%7B%22op%22%3A%22in%22%2C%22content%22%3A%7B%22field%22%3A%22files.analysis.workflow_type%22%2C%22value%22%3A%5B%22MuTect2%20Annotation%22%5D%7D%7D%2C%7B%22op%22%3A%22in%22%2C%22content%22%3A%7B%22field%22%3A%22files.data_category%22%2C%22value%22%3A%5B%22simple%20nucleotide%20variation%22%5D%7D%7D%2C%7B%22op%22%3A%22in%22%2C%22content%22%3A%7B%22field%22%3A%22files.data_format%22%2C%22value%22%3A%5B%22vcf%22%5D%7D%7D%2C%7B%22op%22%3A%22in%22%2C%22content%22%3A%7B%22field%22%3A%22files.data_type%22%2C%22value%22%3A%5B%22Annotated%20Somatic%20Mutation%22%5D%7D%7D%2C%7B%22op%22%3A%22in%22%2C%22content%22%3A%7B%22field%22%3A%22files.experimental_strategy%22%2C%22value%22%3A%5B%22WXS%22%5D%7D%7D%5D%7D) 2022-11-11.

Manually mapped primary sites between TCGA and COSMIC for primary sites with >= 50 samples with CTNNB1 missense mutations in the target region.

* [case_counts_by_primary_site.txt](../../data/tcga/case_counts_by_primary_site.txt)
* [tcga_exomes_by_matched_primary_site.txt](../../analysis/cosmic/tcga_exomes_by_matched_primary_site.txt)
