#!/usr/bin/perl -w

=head1 NAME

fetch_changes.pl

=head1 AUTHOR

Alison Meynert (alison.meynert@ed.ac.uk)

=head1 DESCRIPTION

Identifies reads with codon changes from reference sequence.

=cut

use strict;

# BioPerl
use Bio::DB::Sam;

# Perl
use IO::File;
use File::Basename;
use Getopt::Long;

my $usage = qq{USAGE:
$0 [--help]
  --bam       Input BAM file
  --reference Path to reference sequence FASTA file
  --codons    Codon changes
  --output    Output file
  --contig    Name of reference sequence contig
  --start     Start coordinate of first codon
  --end       End coordinate of last codon
  --cigar     Expected CIGAR string for read with full length matches (no indels or soft clipping)
};

my $help = 0;
my $bam_file;
my $reference_file;
my $codon_file;
my $output = 0;
my $contig;
my $start;
my $end;
my $match_cigar;

GetOptions(
	   'help'        => \$help,
	   'bam=s'       => \$bam_file,
	   'reference=s' => \$reference_file,
           'codons=s'    => \$codon_file,
	   'output=s'    => \$output,
	   'contig=s'    => \$contig,
	   'start=i'     => \$start,
	   'end=i'       => \$end,
           'cigar=s'     => \$match_cigar
) or die $usage;

if ($help || !$bam_file || !$reference_file || !$contig || !$start || !$end || !$codon_file || !$output || !$match_cigar)
{
    print $usage;
    exit(0);
}

# Read in the codon map
my $in_fh = new IO::File;
$in_fh->open($codon_file, "r") or die "Could not open $codon_file\n$!";

my %codons;
while (my $line = <$in_fh>)
{
    chomp $line;
    my ($aa, $codon) = split(',', $line);

    $codons{$codon} = $aa;
}

$in_fh->close();

# open a log file to record reads that don't cover the full region targeted for mutagenesis
my $not_fully_covered_log_fh = new IO::File;
my ($sample, $path, $suffix) = fileparse($bam_file, '.bam');
$not_fully_covered_log_fh->open("$output/$sample.reads_not_fully_covering_region.log", 'w') or die "Could not open $output/$sample.reads_not_fully_covering_region.log\n$!";

# open a log file to record reads with unexpected codons
my $unexpected_codon_log_fh = new IO::File;
$unexpected_codon_log_fh->open("$output/$sample.reads_with_unexpected_codons.log", 'w') or die "Could not open $output/$sample.reads_with_unexpected_codons.log\n$!";

# open the BAM file
my $sam = Bio::DB::Sam->new(-bam => $bam_file, -fasta=> $reference_file);

my @alns = $sam->get_features_by_location(-type=>'match', -seq_id=>$contig, -start=>$start, -end=>$end);

my %reads_with_mutated_codons;
my %reads_with_unexpected_codons;
my %reads_with_offtarget_mutations;
my %all_reads;
my %codon_pos_to_nt_pos;
for (my $i = 0; $i < scalar(@alns); $i++)
{
    # skip all alignments with indels
    next if ($alns[$i]->cigar_str ne $match_cigar);

    my $query_name = $alns[$i]->query->name;
    my ($ref, $matches, $query) = $alns[$i]->padded_alignment;
    my $aln_start = $alns[$i]->start;
    my $aln_end = $alns[$i]->end;

    # record all reads
    $all_reads{$query_name}{'recorded'}++;

    # skip alignments that don't cover the full region targeted for mutagenesis
    if ($aln_start > $start || $aln_end < $end)
    {
	$all_reads{$query_name}{'not_fully_covered'}++;
	print $not_fully_covered_log_fh "$query_name\n";
	next;
    }

    my @ref_seq = split(//, $ref);
    my @query_seq = split(//, $query);
    my $pos = $aln_start;

    my $j = 0;
    my $codon_pos = 1;
    while ($j < scalar(@ref_seq))
    {
	# look only at the area that was targeted for mutagenesis
	if ($pos >= $start && $pos <= $end)
	{
	    # map nucleotide position to codon position
	    $codon_pos_to_nt_pos{$codon_pos} = $pos;

	    # make sure we don't go off the end of the alignment - shouldn't happen as have ensured full coverage
	    if ($j + 2 < scalar(@ref_seq))
	    {
		# compare the reference triplet to the query triplet
		my $ref_codon = $ref_seq[$j] . $ref_seq[$j+1] . $ref_seq[$j+2];
		my $query_codon = $query_seq[$j] . $query_seq[$j+1] . $query_seq[$j+2];
		if ($ref_codon ne $query_codon)
		{
		    if (exists($codons{$query_codon}))
		    {
			# record the new amino acid at this codon position
			$all_reads{$query_name}{'mutation'}++;
			$reads_with_mutated_codons{$query_name}{$codon_pos} = $query_codon;
		    }
		    else
		    {
			# unexpected codon usage - flag it up
			$all_reads{$query_name}{'unexpected_codon'}++;
			$reads_with_unexpected_codons{$query_name}++;
			print $unexpected_codon_log_fh "$query_name\t$pos\t$query_codon\n";
		    }
		}
	    }
	    else
	    {
		# flag it up
		print STDERR "Unexpectedly gone off the end of the alignment: $query_name\n";
		exit(1);
	    }

	    # increment positions by 3 to move to next codon
	    $j+=3;
	    $pos+=3;
	    $codon_pos++;
	}
	else
	{
	    # record reads with mutations outside the target region
	    if ($query_seq[$j] ne $ref_seq[$j])
	    {
		$reads_with_offtarget_mutations{$query_name}{$pos} = $query_seq[$j];
	    }

	    # increment positions by one - no need to worry about indels as there are none
	    $j++;
	    $pos++;
	}
    }
}

$not_fully_covered_log_fh->close();
$unexpected_codon_log_fh->close();

# open a log file to record reads did fully cover the region but were not mutated at all
my $unmutated_log_fh = new IO::File;
$unmutated_log_fh->open("$output/$sample.reads_with_no_mutations.log", 'w') or die "Could not open $output/$sample.reads_with_no_mutations.log\n$!";

foreach my $query_name (keys %all_reads)
{
    # if the only entry for the read is that it was recorded, output it
    if (scalar(keys %{ $all_reads{$query_name} }) == 1)
    {
	print $unmutated_log_fh "$query_name\n";
    }
}

$unmutated_log_fh->close();

# open a log file to record reads with mutations outside the target region
my $offtarget_log_fh = new IO::File;
$offtarget_log_fh->open("$output/$sample.reads_with_offtarget_mutations.log", 'w') or die "Could not open $output/$sample.reads_with_offtarget_mutations.log\n$!";

foreach my $query_name (keys %reads_with_offtarget_mutations)
{
    foreach my $pos (keys %{ $reads_with_offtarget_mutations{$query_name} })
    {
	printf $offtarget_log_fh "$query_name\t$pos\t%s\n", $reads_with_offtarget_mutations{$query_name}{$pos};
    }
}

$offtarget_log_fh->close();


# open a log file to record reads with multiple mutated codons
my $multiple_codon_log_fh = new IO::File;
$multiple_codon_log_fh->open("$output/$sample.reads_with_multiple_codons.log", 'w') or die "Could not open $output/$sample.reads_with_multiple_codons.log\n$!";

my %counts;
foreach my $query_name (keys %reads_with_mutated_codons)
{
    # skip reads that are already recorded as having unexpected codons
    next if (exists($reads_with_unexpected_codons{$query_name}));

    my @pos = keys %{ $reads_with_mutated_codons{$query_name} };

    if (scalar(@pos) > 1)
    {
	print $multiple_codon_log_fh "$query_name\n";
	next;
    }
    
    my $codon = $reads_with_mutated_codons{$query_name}{$pos[0]};
    $counts{$pos[0]}{$codon}++;
}

$multiple_codon_log_fh->close();

# open the output file - one expected mutated codon per read
my $output_fh = new IO::File;
$output_fh->open("$output/$sample.mutations.txt", 'w') or die "Could not open $output/$sample.mutations.txt\n$!";

print $output_fh "codon_pos\tnt_pos\tcodon\taa\tcount\n";

foreach my $pos (sort { $a <=> $b } keys %counts)
{
    foreach my $codon (sort keys %{ $counts{$pos} })
    {
	printf $output_fh "$pos\t%d\t$codon\t%s\t%d\n", $codon_pos_to_nt_pos{$pos}, $codons{$codon}, $counts{$pos}{$codon};
    }
}

$output_fh->close();
