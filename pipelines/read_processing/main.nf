#!/usr/bin/env nextflow

def helpMessage() {
    log.info"""
    Usage:

    The typical command for running the pipeline is as follows:

    nextflow run betacatenin-saturation-screen/pipelines/read_processing/main.nf --input samples.csv --reference ref.fa

    Arguments:
      --input                       Sample sheet CSV file: sample,read1,read2
      --output                      The output directory where the results will be saved
      --reference                   Reference against which to align
      --codons                      CSV mapping amino acids to codons used in the mutational screen
      --contig                      Name of reference contig
      --start                       Start position of first potentially mutated codon in reference contig
      --end                         End position of last potentially mutated codon in reference contig
      --cigar                       Expected cigar string of a read completely matching the reference (i.e. no indels), e.g. 102M

    """.stripIndent()
}

// Show help emssage
params.help = false
if (params.help){
    helpMessage()
    exit 0
}


// Defines reads and outputdir
params.input = "samples.csv"
params.output = "output"
params.reference = "ref.fa"
params.codons = "codons.csv"
params.contig = "Ctnnb1_ref"
params.start = 58
params.end = 117
params.cigar = "102M"

// Header 
println "========================================================"
println "       READ_PROCESSING    P I P E L I N E         "
println "========================================================"
println "['Pipeline Name']     = read_processing"
println "['Pipeline Version']  = workflow.manifest.version"
println "['Input']             = $params.input"
println "['Output dir']        = $params.output"
println "['Reference seq']     = $params.reference"
println "['Codons']            = $params.codons"
println "['Contig']            = $params.contig"
println "['Codon start']       = $params.start"
println "['Codon end']         = $params.end"
println "['Cigar string']      = $params.cigar"
println "['Working dir']       = workflow.workDir"
println "['Container Engine']  = workflow.containerEngine"
println "['Current home']      = $HOME"
println "['Current user']      = $USER"
println "['Current path']      = $PWD"
println "['Working dir']       = workflow.workDir"
println "['Script dir']        = workflow.projectDir"
println "['Config Profile']    = workflow.profile"
println "========================================================"

if (!params.input) {
    exit 1, "Input samplesheet not specified"
}

if (!params.reference) {
    exit 1, "Reference sequence FASTA file not specified"
}

if (!params.output) {
    exit 1, "Output directory not specified"
}

if (!params.codons) {
    exit 1, "Codon file not specified"
}

/*
 * Create value channels for input files
 */
reference_ch = Channel.value(file(params.reference))
codons_ch = Channel.value(file(params.codons))

/*
 * Create a channel for input FASTQ files from the sample sheet CSV
 */
Channel.from(file(params.input))
        .splitCsv()
        .map { row ->
            def sample  = row[0]
            def read1   = returnFile(row[1])
            def read2   = returnFile(row[2])
            [sample, read1, read2]
        }
        .set { reads_ch }


// Index the reference sequence for BWA
process index_reference {

  input:
  file reference from reference_ch

  output:
  file("${reference}.*") into bwa_index_ch

  script:
  """
  bwa index ${reference}
  """
}

// Runs NGmerge adapter removal step - read pairs are aligned against
// each other and the 3' overhangs removed
process adapter_removal {

  publishDir "${params.output}/read_merging", mode: 'copy',
    saveAs: {it.indexOf("log") > 0 ? it : null}

  input:
  set val(sample), file(read1), file(read2) from reads_ch

  output:
  set val(sample), file("*_trimmed_1*"), file("*_trimmed_2*") into trimmed_reads_ch
  file("*log") into adaptor_removal_log_ch

  script:
  """
  NGmerge -1 ${read1} -2 ${read2} -o ${sample}_trimmed -a -c ${sample}_adapter_removal.log
  """
}

// Runs NGmerge stitch step - the trimmed read pairs are merged together
process merge_reads {

  publishDir "${params.output}/read_merging", mode: 'copy'

  input:
  set val(sample), file(read1), file(read2) from trimmed_reads_ch

  output:
  set val(sample), file("*merged*") into merged_reads_ch
  file("*log") into merge_reads_log_ch
  file("*failed_merge*") into failed_merge_reads_ch

  script:
  """
  NGmerge -1 ${read1} -2 ${read2} -o ${sample}_merged.fastq.gz -l ${sample}_stitch_reads.log -f ${sample}_failed_merge
  """

}

// Runs bwa mem to align reads against reference
process align {

  publishDir "${params.output}/alignments", mode: 'copy'

  input:
  file(reference) from reference_ch
  file(reference_index) from bwa_index_ch
  set val(sample), file(reads) from merged_reads_ch

  output:
  set val(sample), file("*.bam*") into alignment_ch

  script:
  """
  bwa mem ${reference} ${reads} | samtools view -hb | samtools sort > ${sample}.bam
  samtools index ${sample}.bam
  """
}

// split alignment channel
Channel
    .from alignment_ch
    .into { alignment1_ch; alignment2_ch }

// Runs samtools stats to get basic statistics about the alignments
process align_stats {

  publishDir "${params.output}/alignments", mode: 'copy'

  input:
  set val(sample), file(alignment) from alignment1_ch

  output:
  set val(sample), file("*.stats") into alignment_stats_ch

  script:
  """
  samtools stats ${sample}.bam > ${sample}.bam.stats
  """

}

// Extracts codon mutation information from alignments
process codon_mutations {

  publishDir "${params.output}/mutations", mode: 'copy'

  input:
  file(reference) from reference_ch
  file(codons) from codons_ch
  set val(sample), file(alignment) from alignment2_ch

  output:
  set val(sample), file("*.log") into mutations_log_ch
  set val(sample), file("*.txt") into mutations_output_ch

  script:
  """
  fetch_changes.pl --bam ${sample}.bam --reference ${reference} --codons ${codons} --output . \
                   --contig ${params.contig} --start ${params.start} --end ${params.end} --cigar ${params.cigar}
  """
}

// Return file if it exists
def returnFile(it) {
    if (!file(it).exists()) exit 1, "Missing file in TSV file: ${it}, see --help for more information"
    return file(it)
}

workflow.onComplete { 
    println ( workflow.success ? "Done!" : "Oops .. something went wrong" )
    log.info "Pipeline Complete"
}
