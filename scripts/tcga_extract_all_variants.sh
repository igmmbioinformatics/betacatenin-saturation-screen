#!/usr/bin/bash

cd raw
for file in *.gz
do
  bcftools +split-vep -f "%CHROM\t%POS\t%ID\t%REF\t%ALT\t%SYMBOL\t%Consequence\t%Protein_position\t%HGVSc\t%HGVSp\n" -d $file \
    > ${file%.vep.vcf.gz}.txt
done
