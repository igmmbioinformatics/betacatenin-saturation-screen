#!/usr/bin/python

import sys
import json

with open (sys.argv[1], "r") as f:
  data = json.load(f)

case = dict()

for d in data:
    file_name = d["file_name"]
    data_format = d["data_format"]
    for c in d["cases"]:
        case_id = c["case_id"]
        if case_id not in case:
          case[case_id] = dict()
        if data_format not in case[case_id]:
          case[case_id][data_format] = list()
        case[case_id][data_format].append(file_name)

for case_id in case:
  tsv_files = ','.join(case[case_id]['TSV']) if 'TSV' in case[case_id] else ""
  maf_files = ','.join(case[case_id]['MAF']) if 'MAF' in case[case_id] else ""

  tsv_files_count = len(case[case_id]['TSV']) if 'TSV' in case[case_id] else 0
  maf_files_count = len(case[case_id]['MAF']) if 'MAF' in case[case_id] else 0

  print("{}\t{}\t{}\t{}\t{}".format(case_id, tsv_files_count, tsv_files, maf_files_count, maf_files))

