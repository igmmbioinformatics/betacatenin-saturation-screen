#!/usr/bin/perl -w

use IO::Zlib;
use IO::File;
use Getopt::Long;
use strict;

my $usage = qq{USAGE:
$0 [--help]
  --input  Map of case id to file id
  --vcfs   Path to VCF files
  --output Output file
};

my $help = 0;
my $input_file;
my $vcfs_dir;
my $vcf_suffix = ".CTNNB1.vcf";
my $txt_suffix = ".CTNNB1.txt";
my $output_file;

GetOptions(
    'help'     => \$help,
    'input=s'   => \$input_file,
    'vcfs=s'    => \$vcfs_dir,
    'output=s'  => \$output_file,
    'vcf_suffix=s' => \$vcf_suffix,
    'txt_suffix=s' => \$txt_suffix
    ) or die $usage;

if ($help || !$input_file || !$vcfs_dir || !$output_file)
{
    print $usage;
    exit(0);
}

my $in_fh = new IO::File;
$in_fh->open($input_file, "r") or die "Could not open $input_file\n$!";

my %files;
while (my $line = <$in_fh>)
{
    chomp $line;
    my ($case_id, $file_id) = split(/\t/, $line);

    push(@{ $files{$case_id} }, $file_id);
}

$in_fh->close();

my $out_fh = new IO::File;
$out_fh->open($output_file, "w") or die "Could not open $output_file\n";

foreach my $case_id (keys %files)
{
    my %variants;
    foreach my $file_id (@{ $files{$case_id} })
    {
	my $vcf_fh;
	if ($vcf_suffix =~ /gz$/)
	{
	    $vcf_fh = new IO::Zlib;
	    $vcf_fh->open("$vcfs_dir/$file_id$vcf_suffix", "rb") or die "Could not open $vcfs_dir/$file_id$vcf_suffix\n$!";
	}
	else
	{
	    $vcf_fh = new IO::File;
	    $vcf_fh->open("$vcfs_dir/$file_id$vcf_suffix", "r") or die "Could not open $vcfs_dir/$file_id$vcf_suffix\n$!";
	}

	my $workflow = "";	
	while (my $line = <$vcf_fh>)
	{
	    chomp $line;

	    if ($line =~ /somatic_mutation_calling_workflow,Name=(.+),Description/)
	    {
		$workflow = $1;
		last;
	    }
	}

	$vcf_fh->close();

	my $txt_fh = new IO::File;
	$txt_fh->open("$vcfs_dir/$file_id$txt_suffix", "r") or die "Could not open $vcfs_dir/$file_id$txt_suffix\n$!";

	while (my $line = <$txt_fh>)
	{
	    chomp $line;

	    my ($chrom, $pos, $id, $ref, $alt, $rest) = split(/\t/, $line, 6);
	    my $variant = "$chrom\t$pos\t$ref\t$alt";
	    $variants{$variant}{$workflow} = $line;
	}
	
	$txt_fh->close();
    }

    foreach my $variant (keys %variants)
    {
	# output if 2 or more workflows agree
	my @workflows = keys %{ $variants{$variant} };
	if (scalar(@workflows) >= 2)
	{
	    print $out_fh "$case_id\t$variants{$variant}{$workflows[0]}\n";
	}
    }
}

$out_fh->close();
