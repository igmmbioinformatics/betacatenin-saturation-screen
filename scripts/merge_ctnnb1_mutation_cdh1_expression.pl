#!/usr/bin/perl -w

use IO::File;
use List::Util qw(sum);
use strict;

sub mean {
    return sum(@_)/@_;
}

my $ctnnb1_file = shift;
my $cdh1_file = shift;
my $map_file = shift;

my $in_fh = new IO::File;
$in_fh->open($ctnnb1_file, "r") or die "Could not open $ctnnb1_file\n$!";

my %ctnnb1_aachange;
while (my $line = <$in_fh>)
{
    chomp $line;
    my ($maf_file, $aachange) = split(/\t/, $line);
    $ctnnb1_aachange{$maf_file . ".gz"} = $aachange;
}

$in_fh->close();

$in_fh->open($cdh1_file, "r") or die "Could not open $cdh1_file\n$!";

my %cdh1_expr;
while (my $line = <$in_fh>)
{
    next if $line =~ /tpm/;

    chomp $line;
    my @tokens = split(/\s+/, $line);
    $cdh1_expr{$tokens[0]} = $tokens[4];
}

$in_fh->close();

$in_fh->open($map_file, "r") or die "Could not open $map_file\n$!";

while (my $line = <$in_fh>)
{
    chomp $line;
    my ($case_id, $tsv_count, $tsv_files, $maf_count, $maf_files) = split(/\t/, $line);

    next if ($tsv_count == 0 || $maf_count == 0);

    my @expr = ();
    my @tsv_file = split(/,/, $tsv_files);
    foreach my $file (@tsv_file)
    {
	if (exists($cdh1_expr{$file}))
	{
	    push(@expr, $cdh1_expr{$file});
	}
    }
    my %aachange;
    my @maf_file = split(/,/, $maf_files);
    foreach my $file (@maf_file)
    {
	if (exists($ctnnb1_aachange{$file}))
	{
	    $aachange{$ctnnb1_aachange{$file}}++;
	}
    }

    if (scalar keys %aachange > 0)
    {
	printf "$case_id\t%s\t%0.4f\n", join(",", keys %aachange), mean(@expr);
    }
}

