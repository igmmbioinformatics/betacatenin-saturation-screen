#!/usr/bin/perl -w

use IO::File;
use List::Util qw(sum);
use strict;

sub mean {
    return sum(@_)/@_;
}

my $ctnnb1_file = shift;
my $wnt_target_file = shift;
my $map_file = shift;

my $in_fh = new IO::File;
$in_fh->open($ctnnb1_file, "r") or die "Could not open $ctnnb1_file\n$!";

my %ctnnb1_aachange;
while (my $line = <$in_fh>)
{
    chomp $line;
    my ($maf_file, $aachange) = split(/\t/, $line);
    $ctnnb1_aachange{$maf_file . ".gz"} = $aachange;
}

$in_fh->close();

$in_fh->open($wnt_target_file, "r") or die "Could not open $wnt_target_file\n$!";

my @wnt_targets;
while (my $line = <$in_fh>)
{
    chomp $line;
    my ($id, $name) = split(/\s+/, $line);
    push(@wnt_targets, $name);
}

$in_fh->close();

my %target_expr;

foreach my $gene (@wnt_targets)
{
    my $target_file = $gene . "_expression.txt";
    $in_fh->open($target_file, "r") or die "Could not open $target_file\n$!";

    while (my $line = <$in_fh>)
    {
	next if $line =~ /tpm/;
	
	chomp $line;
	my @tokens = split(/\s+/, $line);
	$target_expr{$gene}{$tokens[0]} = $tokens[4];
    }

    $in_fh->close();
}

$in_fh->open($map_file, "r") or die "Could not open $map_file\n$!";

printf "case_id\taachange\t%s\n", join("\t", @wnt_targets);

while (my $line = <$in_fh>)
{
    chomp $line;
    my ($case_id, $tsv_count, $tsv_files, $maf_count, $maf_files) = split(/\t/, $line);

    next if ($tsv_count == 0 || $maf_count == 0);

    my @tsv_file = split(/,/, $tsv_files);
    my %gene_expr;
    foreach my $gene (@wnt_targets)
    {
	$gene_expr{$gene} = 0;
	if (exists($target_expr{$gene}))
	{
	    my @expr;

	    foreach my $file (@tsv_file)
	    {
		if (exists($target_expr{$gene}{$file}))
		{
		    push(@expr, $target_expr{$gene}{$file});
		}
	    }
	    
	    if (scalar(@expr) > 0)
	    {
		$gene_expr{$gene} = mean(@expr);
	    }
	}
    }
    my %aachange;
    my @maf_file = split(/,/, $maf_files);
    foreach my $file (@maf_file)
    {
	if (exists($ctnnb1_aachange{$file}))
	{
	    $aachange{$ctnnb1_aachange{$file}}++;
	}
    }

    if (scalar keys %aachange > 0)
    {
	printf "$case_id\t%s", join(",", keys %aachange);

	foreach my $gene (@wnt_targets)
	{
	    printf "\t%0.4f", $gene_expr{$gene};
	}

	print "\n";
    }
}

