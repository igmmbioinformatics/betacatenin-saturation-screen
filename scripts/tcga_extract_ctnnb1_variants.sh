#!/usr/bin/bash

cd raw
for file in *.gz
do
bcftools view -r chr3:41194741-41260096 $file > ../${file%.vep.vcf.gz}.CTNNB1.vcf
done

for file in *.vcf
do
  bcftools +split-vep -f "%CHROM\t%POS\t%ID\t%REF\t%ALT\t%SYMBOL\t%Consequence\t%Protein_position\t%HGVSc\t%HGVSp\n" -d $file \
    | grep ENSP00000344456.5 > ${file%.vcf}.txt
done

