# Based on https://www.biostars.org/p/9500223/

library(data.table)
generate_count_mat <- function(path, pattern) {
  files = list.files(path, pattern, full.names = TRUE, recursive=TRUE, include.dirs=TRUE)
  mat = as.data.frame(do.call(cbind, lapply(files, function(x) fread(x, stringsAsFactors = FALSE))))
  mat <- mat[-c(1:4),]
  gene_type <- as.character(mat[,3])
  rownames(mat) = mat[,1]
  mat = as.data.frame(mat[, seq(4, ncol(mat), 9)])
  mat$gene_type <- gene_type
  mat <- subset(mat, mat$gene_type == "protein_coding")
  mat <- mat[,-c(ncol(mat))]
  return(mat)
}

library(tidyverse)
mrna_counts <- generate_count_mat("data", "\\.rna_seq.augmented_star_gene_counts.tsv$")
file_names <- list.files("data", "\\.rna_seq.augmented_star_gene_counts.tsv$", full.names = FALSE, recursive = TRUE, include.dirs = FALSE)
file_names <- sub(".*/", "", file_names)
meta = read.table("cases.txt", header=F, stringsAsFactors=F, col.names=c("aachange", "case", "project", "mes", "mes.class", "case.id", "file.name"), sep="\t")
meta <- meta[match(file_names, meta$file.name),]
meta = subset(meta, select=c("case.id", "mes.class"))
meta_c<-meta[complete.cases(meta),]
rownames(meta_c) <- NULL
meta_c <- column_to_rownames(meta_c, var="case.id")

colnames(mrna_counts) <- meta$case.id
#colnames(mrna_counts) = substr(colnames(mrna_counts), 1, 12)
#rownames(meta) <- NULL
#meta <- column_to_rownames(meta, var="case.id")



mrna_counts_c<-mrna_counts[!is.na(names(mrna_counts))]




library(DESeq2)

dds <- DESeqDataSetFromMatrix(mrna_counts_c, colData = meta_c, design = ~ mes.class)
dds$mes.class <- relevel(dds$mes.class, ref = "Low")
dds <- DESeq(dds)
res <- results(dds, alpha = 0.05)
res_df_no_shrink <- as.data.frame(res)
#res_norm_shrunk <- lfcShrink(dds, res=res, type = 'normal', coef = 2)
res_ashr_shrunk<-lfcShrink(dds, contrast=c("mes.class","High","Low"), type="ashr",svalue=TRUE)

#res_ashr_shrunk_pval<-lfcShrink(dds, contrast=c("mes.class","High","Low"), type="ashr",svalue=FALSE)

res_merged<-merge(res_df_no_shrink,as.data.frame(res_ashr_shrunk),by=0)

res_merged_cleaned<-res_merged[-grep("PAR_Y",res_merged$Row.names),,drop=FALSE]
rownames(res_merged_cleaned)<-substr(res_merged_cleaned$Row.names,1,15)

library(biomaRt)
ensembl = useEnsembl(biomart="ensembl", dataset="hsapiens_gene_ensembl")
gene_annotation <-getBM(attributes = c("ensembl_gene_id","gene_biotype","external_gene_name","chromosome_name","start_position","end_position"),filters = "ensembl_gene_id",values = rownames(res_merged_cleaned),mart = ensembl)
rownames(gene_annotation)<-gene_annotation$ensembl_gene_id

res_merged_cleaned_annot<-merge(res_merged_cleaned[,-1],gene_annotation[,-1],by=0)
#write.table(res_merged_cleaned_annot,sep="\t",row.names = F,file="High_vs_Low_results_annot.txt")

library(EnhancedVolcano)

png("UCEC_mes_dex_volcano.png", width=600, height=600)
EnhancedVolcano(res, lab = rownames(res), x = 'log2FoldChange', y = 'pvalue', title = "Mutational effect score High vs Low", subtitle = "TCGA-UCEC")
dev.off()

png("LIHC_mes_dex_volcano.png", width=600, height=600)
EnhancedVolcano(res, lab = rownames(res), x = 'log2FoldChange', y = 'pvalue', title = "Mutational effect score High vs Low", subtitle = "TCGA-LIHC")
dev.off()


annot_table<-read.table(file="test_results_ashr_annot.txt",sep="\t",header = TRUE)

EnhancedVolcano(annot_table,
                lab = annot_table$external_gene_name,
                x = 'log2FoldChange',
                y = 'svalue',
                pCutoff = 0.05,
                title = "Mutational effect score High vs Low", subtitle = "TCGA-LIHC",
                #                      title = paste(compare,"_vs_",reference,"_svalues",sep=""),
                legendLabels = c("NS", expression(Log[2] ~ FC), "svalue", expression("svalue and" ~ log[2] ~ FC)),
                
                FCcutoff = 1,
                #                                      xlim = c(-5, 8),
                                ylim = c(0,10),
                labSize = 4.0,
               
                drawConnectors = TRUE)



ncounts<-counts(dds, normalized=TRUE)
