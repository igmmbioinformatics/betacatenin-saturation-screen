#!/usr/bin/perl -w

use IO::File;
use Getopt::Long;
use strict;

my $usage = qq{USAGE:
$0 [--help]
  --input  COSMIC file
  --header MAF file header
  --aamap  File mapping 3 letter to 1 letter amino acid codes
  --output Output file
};

my $help = 0;
my $input_file;
my $header_file;
my $aamap_file;
my $output_file;

GetOptions(
    'help'      => \$help,
    'input=s'   => \$input_file,
    'header=s'  => \$header_file,
    'aamap=s'   => \$aamap_file,
    'output=s'  => \$output_file
    ) or die $usage;

if ($help || !$input_file || !$header_file || !$aamap_file || !$output_file)
{
    print $usage;
    exit(0);
}

# map of COSMIC MUTATION_DESCRIPTION to MAF
# note there are 1 complex compound substitution, 1 complex frameshift, and 3 'frameshift' mutations which may be questionably mapped
my %variant_class = (
    'Complex - compound substitution' => 'Missense_Mutation',
    'Complex - deletion inframe' => 'In_Frame_Del',
    'Complex - frameshift' => 'Frame_Shift_Ins',
    'Deletion - Frameshift' => 'Frame_Shift_Del',
    'Deletion - In frame' => 'In_Frame_Del',
    'Frameshift' => 'Frame_Shift_Ins',
    'Insertion - Frameshift' => 'Frame_Shift_Ins',
    'Insertion - In frame' => 'In_Frame_Ins',
    'Nonstop extension' => 'Nonstop_Mutation',
    'null' => '.',
    'Substitution - coding silent' => 'Silent',
    'Substitution - Missense' => 'Missense_Mutation',
    'Substitution - Nonsense' => 'Nonsense_Mutation',
    'Unknown' => '.');

# this may obscure some edge cases
my %variant_type = (
    'Complex - compound substitution' => 'ONP',
    'Complex - deletion inframe' => 'DEL',
    'Complex - frameshift' => 'INS',
    'Deletion - Frameshift' => 'DEL',
    'Deletion - In frame' => 'DEL',
    'Frameshift' => 'INS',
    'Insertion - Frameshift' => 'INS',
    'Insertion - In frame' => 'INS',
    'Nonstop extension' => 'SNP',
    'null' => '.',
    'Substitution - coding silent' => 'SNP',
    'Substitution - Missense' => 'SNP',
    'Substitution - Nonsense' => 'SNP',
    'Unknown' => '.');

my $head_fh = new IO::File;
$head_fh->open($header_file, "r") or die "Could not open $header_file\n$!";

my @colnames;
while (my $line = <$head_fh>)
{
    chomp $line;
    @colnames = split(/\t/, $line);
}
$head_fh->close();

my $aamap_fh = new IO::File;
$aamap_fh->open($aamap_file, "r") or die "Could not open $aamap_file\n$!";

my %aamap;
while (my $line = <$aamap_fh>)
{
    chomp $line;
    my ($codon, $aa3, $aa1, $aafull) = split(/\t/, $line);
    $aamap{$aa3} = $aa1;
}
$head_fh->close();


my $in_fh = new IO::File;
$in_fh->open($input_file, "r") or die "Could not open $input_file\n$!";

my $out_fh = new IO::File;
$out_fh->open($output_file, "w") or die "Could not open $output_file\n";

print $out_fh join("\t", @colnames);
print $out_fh "\n";

my %output;
while (my $line = <$in_fh>)
{
    next if ($line =~ /^GENE_NAME/);

    chomp $line;
    my @tokens = split(',', $line);

    for (my $i = 0; $i < scalar(@colnames); $i++)
    {
	$output{$colnames[$i]} = '.';
    }

    # skip variants without HGVSp annotation
    next if ($tokens[37] !~ /^ENSP/);

    $tokens[37] =~ /ENSP\d+\.\d+:(p\..*)/;
    my $hgvsp = $1;

    $hgvsp =~ /(\d+)/;
    my $cds_pos = $1;

    foreach my $aa3 (keys %aamap)
    {
	$hgvsp =~ s/$aa3/$aamap{$aa3}/g;
    }

    $output{'Hugo_Symbol'} = $tokens[0];
    $output{'HGVSp_Short'} = $hgvsp;
    $output{'Variant_Classification'} = $variant_class{$tokens[21]};
    $output{'Variant_Type'} = $variant_type{$tokens[21]};
    $output{'CDS_position'} = $cds_pos;

    # skip variants that can't be classified
    next if ($output{'Variant_Classification'} eq '.');

    print $out_fh "$output{$colnames[0]}";
    for (my $i = 1; $i < scalar(@colnames); $i++)
    {
	print $out_fh "\t$output{$colnames[$i]}";
    }
    print $out_fh "\n";
}

$in_fh->close();
$out_fh->close();
