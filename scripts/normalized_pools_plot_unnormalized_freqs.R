source("../../data/resources/palette.R")
source("../../data/resources/multiplot.R")

p = read.table("population_frequencies.csv", header=T, sep=",", stringsAsFactors=F)

library(ggplot2)

j = 2; p2freqplot = ggplot(subset(p, p$sample == paste0("p", j)), aes(fill = aa, y = freq, x = ref_label)) + geom_bar(position = "stack", stat="identity") + xlab("Codon position") + ylab(sprintf("Frequency in p%d sample", j)) + scale_fill_manual(values = aacols) + labs(fill="AA") + ylim(0,0.25)
j = 3; p3freqplot = ggplot(subset(p, p$sample == paste0("p", j)), aes(fill = aa, y = freq, x = ref_label)) + geom_bar(position = "stack", stat="identity") + xlab("Codon position") + ylab(sprintf("Frequency in p%d sample", j)) + scale_fill_manual(values = aacols) + labs(fill="AA") + ylim(0,0.25)
j = 4; p4freqplot = ggplot(subset(p, p$sample == paste0("p", j)), aes(fill = aa, y = freq, x = ref_label)) + geom_bar(position = "stack", stat="identity") + xlab("Codon position") + ylab(sprintf("Frequency in p%d sample", j)) + scale_fill_manual(values = aacols) + labs(fill="AA") + ylim(0,0.25)
j = 5; p5freqplot = ggplot(subset(p, p$sample == paste0("p", j)), aes(fill = aa, y = freq, x = ref_label)) + geom_bar(position = "stack", stat="identity") + xlab("Codon position") + ylab(sprintf("Frequency in p%d sample", j)) + scale_fill_manual(values = aacols) + labs(fill="AA") + ylim(0,0.25)
j = 6; p6freqplot = ggplot(subset(p, p$sample == paste0("p", j)), aes(fill = aa, y = freq, x = ref_label)) + geom_bar(position = "stack", stat="identity") + xlab("Codon position") + ylab(sprintf("Frequency in p%d sample", j)) + scale_fill_manual(values = aacols) + labs(fill="AA") + ylim(0,0.25)
j = 7; p7freqplot = ggplot(subset(p, p$sample == paste0("p", j)), aes(fill = aa, y = freq, x = ref_label)) + geom_bar(position = "stack", stat="identity") + xlab("Codon position") + ylab(sprintf("Frequency in p%d sample", j)) + scale_fill_manual(values = aacols) + labs(fill="AA") + ylim(0,0.25)

png("population_freq_by_codon_and_position.png", width=1600, height=1200)
multiplot(p2freqplot, p3freqplot, p4freqplot, p5freqplot, p6freqplot, p7freqplot, cols=2, layout=matrix(c(1,2,3,4,5,6), nrow=3, byrow=TRUE))
dev.off()
