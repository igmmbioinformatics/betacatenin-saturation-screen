#!/usr/bin/python

import sys
import json

with open (sys.argv[1], "r") as f:
  data = json.load(f)

for vcf in data:
    file_name = vcf["file_name"]
    for case in vcf["cases"]:
        case_id = case["case_id"]
        print("{}\t{}".format(case_id, file_name))
