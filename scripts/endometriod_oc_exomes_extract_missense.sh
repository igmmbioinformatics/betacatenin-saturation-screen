#!/usr/bin/bash

# Extract relevant info from VCFs

echo "sample,chr,pos,id,ref,alt,gene,csq,protein_pos,hgvsc,hgvsp,gnomad" | sed -e 's/,/\t/g' > variants.CTNNB1.txt 
for id in `cat endometriod_oc_sample_ids.txt`
do
  bcftools +split-vep \
    -f "%CHROM\t%POS\t%ID\t%REF\t%ALT\t%SYMBOL\t%Consequence\t%Protein_position\t%HGVSc\t%HGVSp\t%gnomAD_AF\n" \
    -d ../../data/endometriod_ovarian/$id.endometriod_oc_exome-ensemble-annotated.CTNNB1.vcf \
    | awk -v ID=$id '{ print ID "\t" $0 }' \
    | sed -e 's/\/781//' \
    | grep CTNNB1 \
    >> variants.CTNNB1.txt
done

# Select missense variants at positions 31 to 48

Rscript ../../scripts/endometriod_oc_exomes_select_missense_31-48.R

# Remove transcript & gene ids

sed -e 's/ENST00000349496.9:c.//' variants.CTNNB1.missense.intarget.csv \
  | sed -e 's/ENSP00000344456.5:p.//' > temp.out
mv temp.out variants.CTNNB1.missense.intarget.csv 

# Map 3 AAA codes to 1 letter

Rscript ../../scripts/endometriod_oc_exomes_map_3AA_codes_to_1AA_codes.R

# Plot missense mutations

Rscript ../../scripts/endometriod_oc_exomes_plot_missense.R
