#!/usr/bin/bash

echo "Liver duplicate cases"

cut -f 1 files.2021-09-15_liver.txt | sort | uniq -c | sort -nr | head

echo
echo "Uterine duplicate cases"

cut -f 1 files.2021-09-15_uterine.txt | sort | uniq -c | sort -nr | head
