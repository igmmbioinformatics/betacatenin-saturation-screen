#!/usr/bin/bash

# Copy data from Gourley lab fileset

mkdir -p data/endometriod_ovarian
cd data/endometriod_ovarian
ls /exports/igmm/datastore/Genetics-lab/EndometrioidOC-exome/bcbio/upload > ../../analysis/endometriod_ovarian/endometriod_oc_sample_ids.txt

for id in `cat ../../analysis/endometriod_ovarian/endometriod_oc_sample_ids.txt`
do
bcftools view -r chr3:41194741-41260096 \
  /exports/igmm/datastore/Genetics-lab/EndometrioidOC-exome/bcbio/upload/$id/*/endometriod_oc_exome-ensemble-annotated.vcf.gz \
  > $id.endometriod_oc_exome-ensemble-annotated.CTNNB1.vcf
done

# Variant summary

cat *.vcf | grep -v ^# | cut -f 1-3 | sort | uniq -c > ../../analysis/endometriod_ovarian/variant_summary.txt
