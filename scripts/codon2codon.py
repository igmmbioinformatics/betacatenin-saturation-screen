#!/usr/bin/env python
from __future__ import print_function
import re
import os
import sys
import shutil
import collections
from optparse import OptionParser
from Bio.Seq import Seq

nts = ['A', 'C', 'G', 'T']

full_path_file = None
per_path_freq_file = None
per_mutation_freq_file = None

path_freqs = dict()

def generate_codons(codons, prefix, length):

    if (len(prefix) == length):
        codons.append(prefix)
        return

    for nt in nts:
        generate_codons(codons, prefix + nt, length)

def revcomp(seq):
    sequence = Seq(seq)
    return sequence.reverse_complement()

def mutate(freq_map, source_codon_in_context, source_codon, target_codon, prev_source_actual_mutation, prev_target_actual_mutation, prev_index, path):

    new_path = path.copy()

    freq = 0
    if ("{}:{}".format(prev_source_actual_mutation, prev_target_actual_mutation) in freq_map):
        freq = freq_map.get("{}:{}".format(prev_source_actual_mutation, prev_target_actual_mutation))
    else:
        freq = freq_map.get("{}:{}".format(revcomp(prev_source_actual_mutation), revcomp(prev_target_actual_mutation)))
    
    new_path.append("{}:{}:{}:{}:{}:{}:{}".format(source_codon_in_context, source_codon, target_codon,
                                                      prev_source_actual_mutation, prev_target_actual_mutation,
                                                      freq, prev_index))

    if (source_codon == target_codon):

        if (len(new_path) > 1):

            start_source_codon_in_context = new_path[0].split(":")[0]
            start_source_codon = new_path[0].split(":")[1]
            end_target_codon = new_path[0].split(":")[2]
            freq = 1
            for i in range(1,len(new_path)):
                freq *= float(new_path[i].split(":")[5])
            steps = len(new_path) - 1

            print("{}\t{}\t{}\t{}\t{}".format(start_source_codon_in_context, start_source_codon, end_target_codon, steps, freq), file = per_path_freq_file)
            print(new_path, file = full_path_file)

            codon_key = "{}\t{}\t{}".format(start_source_codon_in_context, start_source_codon, end_target_codon)
            if codon_key in path_freqs:
                path_freqs[codon_key].append(float(freq))
            else:
                path_freqs[codon_key] = [float(freq)]

        return

    for i in range(len(source_codon)):
        if source_codon[i] == target_codon[i]:
            continue

        new_source_codon_in_context = source_codon_in_context[0:i+1] + target_codon[i] + source_codon_in_context[i+2:len(source_codon_in_context)]
        new_source_codon = source_codon[0:i] + target_codon[i] + source_codon[i+1:len(source_codon)]

        source_actual_mutation = source_codon_in_context[i:i+3]
        target_actual_mutation = new_source_codon_in_context[i:i+3]
        
        mutate(freq_map, new_source_codon_in_context, new_source_codon, target_codon, source_actual_mutation, target_actual_mutation, i, new_path)

def generate_codon_paths(freq_map):

    codons_in_context = []
    generate_codons(codons_in_context, "", 5)

    codons = []
    generate_codons(codons, "", 3)

    for i in range(len(codons_in_context)):
        source_codon_in_context = codons_in_context[i]
        source_codon = source_codon_in_context[1:4]

        for j in range(len(codons)):
            path = []
            target_codon = codons[j]
            mutate(freq_map, source_codon_in_context, source_codon, target_codon, "", "", -1, path)

def read_background_frequencies(input_file):

    freq_map = {}
    
    with open(input_file) as f:
        for line in f:
            tokens = line.split()
            mutation = tokens[0]
            prefix = mutation[0]
            suffix = mutation[len(mutation)-1]

            source = mutation[2]
            target = mutation[4]

            source_codon = "{}{}{}".format(prefix, source, suffix)
            target_codon = "{}{}{}".format(prefix, target, suffix)

            freq = tokens[3]
            freq_map.update({"{}:{}".format(source_codon, target_codon) : freq })

    return freq_map

def read_codon_map(input_file):

    codon_map = {}
    
    with open(input_file) as f:
        for line in f:
            (codon, aa3, aa1, aa) = line.split()
            codon_map.update({ codon : aa1 })

    return codon_map

if __name__ == "__main__":
    
    parser = OptionParser(description="""Generates the codon to codon change paths given tri-nucleotide context""")
    parser.add_option("-b", "--background", dest="background_file_name", help="Background frequencies file path", metavar="FILE", action="store", type="string")
    parser.add_option("-o", "--output", dest="output_file_prefix", help="Output file path prefix", default="output_codon2codon", action="store", type="string")
    parser.add_option("-c", "--codon", dest="codon_map_file_name", help="Codon to amino acid map file path", metavar="FILE", action="store", type="string")

    (options, args) = parser.parse_args()

    if options.background_file_name is None:
        parser.print_help()
        parser.error("Background file path is required")
    
    freq_map = read_background_frequencies(options.background_file_name)
    codon_map = read_codon_map(options.codon_map_file_name)
    
    full_path_file = open("{}_full_paths.txt".format(options.output_file_prefix), "w")
    per_path_freq_file = open("{}_per_path_freqs.txt".format(options.output_file_prefix), "w")
    
    generate_codon_paths(freq_map)
    
    full_path_file.close()
    per_path_freq_file.close()

    per_mutation_freq_file = open("{}_per_mutation_freqs.txt".format(options.output_file_prefix), "w")

    for codon_key in path_freqs.keys():
        (context, source_codon, target_codon) = codon_key.split("\t")
        res = 1
        for freq in path_freqs[codon_key]:
            res *= (1 - freq)
        res = 1 - res
        
        print(codon_key, codon_map[source_codon], codon_map[target_codon], res, file=per_mutation_freq_file)

    per_mutation_freq_file.close()
    
